<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;
use App\Repositories\Tenant\AppointmentRepository;
use App\Http\Requests\Tenant\Appointment\ShowRequest;
use App\Http\Requests\Tenant\Appointment\IndexRequest;
use App\Http\Requests\Tenant\Appointment\StoreRequest;
use App\Http\Requests\Tenant\Appointment\UpdateRequest;
use App\Http\Requests\Tenant\Appointment\DestroyRequest;

class AppointmentController extends Controller
{
    protected $appointmentRepository;

    /**
     * Appointment Constructor.
     *
     * @param AppointmentRepository $appointmentRepository
     */

    public function __construct(AppointmentRepository $appointmentRepository)
    {
      $this->appointmentRepository = $appointmentRepository;
    }

    /**
     * @OA\Get(
     *   path="/appointments",
     *   tags={"[Tenant] Appointments"},
     *   summary="Lists all appointments associated with tenant",
     *   operationId="IndexAppointment",
     *   security={ {"bearer": {}} },
     *
     * @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * )
     **/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        return response()->json($this->appointmentRepository->getAll($request->all()));
    }

    /**
     * @OA\Post(
     ** path="/appointments",
     *   tags={"[Tenant] Appointments"},
     *   summary="Store new appointments",
     *   operationId="StoreAppointment",
     *   security={ {"bearer": {}} },
     *
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new Appointment",
     *     @OA\JsonContent(
     *          required={"urgency","scheduled_date","user_id"},
     *          @OA\Property(property="first_name", type="string", format="string", example="Petra"),
     *          @OA\Property(property="last_name", type="string", format="string", example="Perez"),
     *          @OA\Property(property="dni", type="integer", example="26453454"),
     *          @OA\Property(property="urgency", type="boolean", example=false),
     *          @OA\Property(property="schedule_date", type="string", format="date", example="2021-01-10"),
     *          @OA\Property(property="patient_id", type="integer", example=40),
     *          @OA\Property(property="user_id", type="integer", example=1),
     *    )
     * ),
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return response()->json($this->appointmentRepository->create($request->all()))->setStatusCode(201);
    }

    /**
     * @OA\Get(
     *   path="/appointments/{id}",
     *   tags={"[Tenant] Appointments"},
     *   summary="Details Appointments",
     *   operationId="ShowAppointment",
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, $id)
    {
        return response()->json($this->appointmentRepository->getById($id));
    }

    /**
     * @OA\Put(
     ** path="/appointments/{id}",
     *   tags={"[Tenant] Appointments"},
     *   summary="Update appointments",
     *   operationId="UpdateAppointment",
     *   security={ {"bearer": {}} },
     *
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new Appointment",
     *     @OA\JsonContent(
     *          required={"scheduled_date","user_id"},
     *          @OA\Property(property="first_name", type="string", format="string", example="Petra"),
     *          @OA\Property(property="last_name", type="string", format="string", example="Perez"),
     *          @OA\Property(property="dni", type="integer", example="26453454"),
     *          @OA\Property(property="urgency", type="boolean", example=true),
     *          @OA\Property(property="schedule_date", type="string", format="date", example="2021-01-10"),
     *          @OA\Property(property="patient_id", type="integer", example=40),
     *          @OA\Property(property="user_id", type="integer", example=1),
     *    )
     * ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->appointmentRepository->UpdateById($id, $request->all()));
    }

    /**
     * @OA\Delete(
     *   path="/appointments/{id}",
     *   tags={"[Tenant] Appointments"},
     *   summary="Delete Appointments",
     *   operationId="DeleteAppointments",
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, $id)
    {
        return response()->json($this->appointmentRepository->destroy($id))->setStatusCode(204);
    }
}
