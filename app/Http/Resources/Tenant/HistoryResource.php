<?php

namespace App\Http\Resources\Tenant;

use Illuminate\Http\Resources\Json\JsonResource;

class HistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'cod'              => $this->cod,
            'allergies'        => $this->allergies,
            'medication'       => $this->medication,
            'surgical_history' => $this->surgical_history,
            'risk_factors'     => $this->risk_factors,
            'toxic_habits'     => $this->toxic_habits,
            'consults'         => $this->consultations,
            'created_at'       => ($this->created_at)->format('Y-m-d H:m:s'),
            'updated_at'       => ($this->updated_at)->format('Y-m-d H:m:s'),
        ];
    }
}
