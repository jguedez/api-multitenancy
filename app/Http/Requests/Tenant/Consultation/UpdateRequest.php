<?php

namespace App\Http\Requests\Tenant\Consultation;

use App\Http\Requests\ApiRequest;
use App\Models\Tenant\Consultation;
use App\Policies\Tenant\ConsultationPolicy;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new ConsultationPolicy(request()->header('x-tenant'), 'consultation.update');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameters('consultations');
        $consultation = $this->findRecordInRequest(Consultation::class, $id['consultation']);

        return [
            'cod'                  => 'required|unique:consultations,cod,'.$consultation->id,
            'subject'              => 'sometimes',
            'duration'             => 'sometimes',
            'appointment_id'       => 'sometimes|exists:appointments,id|unique:consultations,appointment_id,'.$consultation->id,
            'histories'            => 'sometimes|exists:histories,id',
            'diagnostics'          => 'sometimes|exists:diagnostics,id',
            'complaints'           => 'sometimes|exists:complaints,id',
            'investigations'       => 'sometimes|exists:investigations,id',
            'treatment_plans'      => 'sometimes|exists:treatment_plans,id',
            'treatment_procedures' => 'sometimes|exists:treatment_procedures,id',
            'drugs'                => 'sometimes|exists:drugs,id',
        ];
    }
}
