<?php

namespace App\Repositories\Tenant;

use App\Models\Tenant\Schedule;
use App\Repositories\BaseInterface;
use App\Http\Resources\Tenant\ScheduleResource;
use App\Http\Resources\Tenant\ScheduleCollection;

class ScheduleRepository implements BaseInterface
{
    protected $model;
    protected $schedule;

    /**
     * Schedule Repository constructor.
     * @param Drug $schedule
     */
    public function __construct(Schedule $schedule)
    {
        $this->model = $schedule;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new ScheduleCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new = $this->model::create($data);
        return new ScheduleResource($new);
    }

    public function getById($id)
    {
        return new ScheduleResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $this->model::findOrFail($id)->update($data);
        return $this->getById($id);
    }
}
