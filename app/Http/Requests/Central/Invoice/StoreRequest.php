<?php

namespace App\Http\Requests\Central\Invoice;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject'                   => 'sometimes',
            'id_payment'                => 'required|unique:invoices,id_payment',
            'due_date'                  => 'required|date',
            'excise_duty'               => 'required',
            'tax'                       => 'required|numeric',
            'currency'                  => 'required|string',
            'shipping_address'          => 'sometimes',
            'shipping_address.street'   => 'required_with:shipping_address',
            'shipping_address.country'  => 'required_with:shipping_address',
            'shipping_address.state'    => 'required_with:shipping_address',
            'shipping_address.city'     => 'required_with:shipping_address',
            'details'                   => 'required',
            'details.article'           => 'required_with:details',
            'details.description'       => 'required_with:details',
            'details.amount'            => 'required_with:details',
            'total'                     => 'required|numeric',
            'subscription_id'           => 'required|exists:subcriptions,id|unique:invoices,subscription_id',
        ];
    }
}
