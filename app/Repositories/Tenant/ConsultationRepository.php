<?php

namespace App\Repositories\Tenant;

use App\Models\Tenant\Consultation;
use App\Repositories\BaseInterface;
use App\Http\Resources\Tenant\ConsultationResource;
use App\Http\Resources\Tenant\ConsultationCollection;

class ConsultationRepository implements BaseInterface
{
    protected $model;
    protected $consultation;

    /**
     * Consultation Repository constructor.
     * @param Consultation $consultation
     */
    public function __construct(Consultation $consultation)
    {
        $this->model = $consultation;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new ConsultationCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new = $this->model::create($data);

        $this->SyncConsultation($data, $this->model::findOrFail($new->id));

        return new ConsultationResource($new);
    }

    public function getById($id)
    {
        return new ConsultationResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $consultation = $this->model::findOrFail($id);
        $consultation->update($data);

        $this->SyncConsultation($data, $consultation);

        return $this->getById($id);
    }

    protected function SyncConsultation($data, $model)
    {
        $model->histories()->sync($data['histories']);
        $model->diagnostics()->sync($data['diagnostics']);
        $model->complaits()->sync($data['complaints']);
        $model->treatmentPlan()->sync($data['treatment_plans']);
        $model->treatmentProcedure()->sync($data['treatment_plans']);
        $model->drugs()->sync($data['treatment_plans']);
        $model->investigation()->sync($data['investigations']);
    }
}
