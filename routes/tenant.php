<?php

declare(strict_types=1);

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Stancl\Tenancy\Middleware\InitializeTenancyByRequestData;

/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here you can register the tenant routes for your application.
| These routes are loaded by the TenantRouteServiceProvider.
|
| Feel free to customize them however you want. Good luck!
|
*/

Route::group([
    'middleware' => [InitializeTenancyByRequestData::class]
], function () {
    Route::resource('drugs', 'Tenant\DrugController')->except(['edit', 'create']);
    Route::resource('diagnostics', 'Tenant\DiagnosticController')->except(['edit', 'create']);
    Route::resource('complaints', 'Tenant\ComplaintController')->except(['edit', 'create']);
    Route::resource('investigations', 'Tenant\InvestigationController')->except(['edit', 'create']);
    Route::resource('treatment_plans', 'Tenant\TreatmentPlanController')->except(['edit', 'create']);
    Route::resource('treatment_procedures', 'Tenant\TreatmentProcedureController')->except(['edit', 'create']);
    Route::resource('histories', 'Tenant\HistoryController')->except(['edit', 'create']);
    Route::resource('patients', 'Tenant\PatientController')->except(['edit', 'create']);
    Route::resource('specialties', 'Tenant\SpecialtyController')->except(['edit', 'create']);
    Route::resource('users', 'Tenant\UserController')->only(['show', 'update', 'destroy']);
    Route::resource('schedules', 'Tenant\ScheduleController')->except(['edit', 'create']);
    Route::resource('appointments', 'Tenant\AppointmentController')->except(['edit', 'create']);
    Route::resource('consultations', 'Tenant\ConsultationController')->except(['edit', 'create']);
});

