<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\Central\PlanSeeder;
use Database\Seeders\Central\TenantSeeder;
use Database\Seeders\Central\InvoiceSeeder;
use Database\Seeders\Central\CentralUserSeeder;
use Database\Seeders\Central\SubscriptionSeeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CentralUserSeeder::class,
            PlanSeeder::class,
            SubscriptionSeeder::class,
            InvoiceSeeder::class,
            TenantSeeder::class,
        ]);


    }
}
