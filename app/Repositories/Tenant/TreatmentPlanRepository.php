<?php

namespace App\Repositories\Tenant;

use App\Repositories\BaseInterface;
use App\Models\Tenant\TreatmentPlan;
use App\Http\Resources\Tenant\TreatmentPlanResource;
use App\Http\Resources\Tenant\TreatmentPlanCollection;

class TreatmentPlanRepository implements BaseInterface
{
    protected $model;
    protected $treatmentPlan;

    /**
     * Treatment Plan Repository constructor.
     * @param TreatmentPlan $treatmentPlan
     */
    public function __construct(TreatmentPlan $treatmentPlan)
    {
        $this->model = $treatmentPlan;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new TreatmentPlanCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new = $this->model::create($data);
        return new TreatmentPlanResource($new);
    }

    public function getById($id)
    {
        return new TreatmentPlanResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $this->model::findOrFail($id)->update($data);
        return $this->getById($id);
    }

}
