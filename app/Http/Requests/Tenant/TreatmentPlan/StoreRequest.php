<?php

namespace App\Http\Requests\Tenant\TreatmentPlan;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\TreatmentPlanPolicy;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new TreatmentPlanPolicy(request()->header('x-tenant'), 'treatmentPlan.store');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'plan'     => 'required',
            'status'   => 'required|boolean',
            'comments' => 'sometimes',
        ];
    }
}
