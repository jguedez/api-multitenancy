<?php

namespace App\Http\Payments;

use MercadoPago\SDK;
use MercadoPago\Payer;

class MPHelper {

    public $payment;

    public function __construct()
    {
        \MercadoPago\SDK::setAccessToken(ENV('ACCESS_TOKEN_MP'));
    }

    public function newPayment(Array $data)
    {
        $this->payment = new \MercadoPago\Payment();

        $this->payment->transaction_amount = $data['transactionAmount'];
        $this->payment->token = $data['token'];
        $this->payment->description = $data['description'];
        $this->payment->installments = 1;
        $this->payment->payment_method_id = $data['paymentMethodId'];
        $this->payment->issuer_id = $data['issuer'];

        $this->payment->payer = $this->setPayer($data);

        $this->payment->save();

        return response()->json([
            'status'        => $this->payment->status,
            'status_detail' => $this->payment->status_detail,
            'id'            => $this->payment->id
        ]);
    }

    protected function setPayer($data)
    {
        $payer = new \MercadoPago\Payer();
        $payer->email = $data['email'];
        $payer->identification = array(
            "type" => $data['docType'],
            "number" => $data['docNumber']
        );


    }
}
