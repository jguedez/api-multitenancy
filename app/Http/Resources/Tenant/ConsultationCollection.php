<?php

namespace App\Http\Resources\Tenant;

use App\Http\Resources\ApiCollection;
use App\Http\Resources\Tenant\ConsultationResource;

class ConsultationCollection extends ApiCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data'  => ConsultationResource::collection($this->collection),
            'meta'  => $this->dataMeta($request),
            'links' => $this->dataLinks($request)
        ];
    }
}
