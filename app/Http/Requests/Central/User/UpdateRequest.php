<?php

namespace App\Http\Requests\Central\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = $this->route()->parameters('users');

        return [
            'first_name'        => 'sometimes',
            'last_name'         => 'sometimes',
            'dni'               => 'required|unique:users,dni,'.$user_id['user'],
            'email'             => 'required|unique:users,email,'.$user_id['user'],
            'password'          => 'sometimes|string|min:6',
            'is_active'         => 'sometimes|boolean',
            'phone'             => 'sometimes',
            'phone.landline'    => 'required_with:phone',
            'phone.movil'       => 'required_with:phone',
            'address'           => 'sometimes',
            'address.street'    => 'required_with:address',
            'address.country'   => 'required_with:address',
            'address.state'     => 'required_with:address',
            'address.city'      => 'required_with:address',
        ];
    }
}
