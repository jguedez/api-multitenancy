<?php

namespace Database\Seeders\Central;

use App\Models\Central\Tenant;
use Illuminate\Database\Seeder;
use App\Models\Central\CentralUser;

class TenantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = CentralUser::find(1);
        $tenant = Tenant::create(['id' => $user->global_id]);
        $tenant = Tenant::find($user->global_id);
        $user->tenants()->attach($tenant);
    }
}
