<?php

namespace Database\Seeders\Tenant;

use App\Models\Tenant\Schedule;
use Illuminate\Database\Seeder;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schedule::factory()->count(3)->create();

    }
}
