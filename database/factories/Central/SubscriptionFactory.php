<?php

namespace Database\Factories\Central;

use App\Models\Central\Subscription;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubscriptionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Subscription::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'start_at'   => now(),
            'ends_at'    => now()->addDays(30),
            'user_id'    => 1,
            'plan_id'    => $this->faker->unique()->numberBetween(1, 3)
        ];
    }
}
