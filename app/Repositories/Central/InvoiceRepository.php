<?php

namespace App\Repositories\Central;

use App\Models\Central\Invoice;
use App\Repositories\BaseInterface;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Central\InvoiceResource;
use App\Http\Resources\Central\InvoiceCollection;

class InvoiceRepository implements BaseInterface
{
    protected $model;
    protected $invoice;

    /**
     * Invoice Repository constructor.
     * @param Invoice $invoice
     */
    public function __construct(Invoice $invoice)
    {
        $this->model = $invoice;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new InvoiceCollection(Auth::user()->invoices()->paginate($page));
    }

    public function create(array $data)
    {
        $new = $this->model::create($data);
        return new InvoiceResource($new);
    }

    public function getById($id)
    {
        return new InvoiceResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $this->model::findOrFail($id)->update($data);
        return $this->getById($id);
    }
}
