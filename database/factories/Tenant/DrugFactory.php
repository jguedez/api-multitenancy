<?php

namespace Database\Factories\Tenant;

use App\Models\Tenant\Drug;
use Illuminate\Database\Eloquent\Factories\Factory;

class DrugFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Drug::class;

    /**
     * Define the model's default state.
     *
     * @return array

     */
    public function definition()
    {
        return [
            'cod'         => 'DRUG-N-'.$this->faker->unique()->randomDigit,
            'description' => $this->faker->sentence(3),
            'dosage'      => $this->faker->word,
            'duration'    => $this->faker->randomDigit.' Cada '.$this->faker->randomDigit.' Dias',
            'dosage_form' => $this->faker->word.' Tabletas',
            'comments'    => $this->faker->sentence(6),
            'created_at'  => now(),
            'updated_at'  => now(),
        ];
    }
}
