<?php

namespace App\Http\Requests\Tenant\Investigation;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\InvestigationPolicy;

class ShowRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new InvestigationPolicy(request()->header('x-tenant'), 'investigation.show');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
