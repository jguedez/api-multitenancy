<?php

namespace App\Http\Requests\Tenant\Schedule;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\SchedulePolicy;

class DestroyRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new SchedulePolicy(request()->header('x-tenant'), 'schedule.destroy');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
