<?php

namespace App\Models\Central;


use App\Models\Central\Subscription;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Plan extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'name', 'description', 'currency',
       'benefits', 'unit_price', 'time_duration',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'benefits' => 'array'
    ];

    /**
     * Get the plan record associated with the subscription.
     */
    public function subscription()
    {
        return $this->hasOne(Subscription::class, 'plan_id', 'id');
    }
}
