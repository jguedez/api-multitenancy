<?php

namespace App\Repositories\Central;

use App\Repositories\BaseInterface;
use App\Models\Central\Subscription;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Central\SubscriptionResource;
use App\Http\Resources\Central\SubscriptionCollection;

class SubscriptionRepository implements BaseInterface
{
    protected $model;
    protected $subscription;

    /**
     * Subscription Repository constructor.
     * @param Subscription $subscription
     */
    public function __construct(Subscription $subscription)
    {
        $this->model = $subscription;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new SubscriptionCollection($this->model::where('user_id', '=', Auth::user()->id)->paginate($page));
    }

    public function create(array $data)
    {
        $new = $this->model::create($data);
        return new SubscriptionResource($new);
    }

    public function getById($id)
    {
        $subscription = new SubscriptionResource($this->model::findOrFail($id));

        if (Auth::user()->id === $subscription->user['id'])
        {
            return $subscription;
        }

        return ['error' => 'You do not have access to this resource'];
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $this->model::findOrFail($id)->update($data);
        return $this->getById($id);
    }
}
