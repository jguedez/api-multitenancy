<?php

namespace App\Providers;

use App\Models\Tenant\Drug;
use App\Models\Tenant\User;
use App\Models\Tenant\History;
use App\Models\Tenant\Patient;
use App\Models\Tenant\Schedule;
use App\Models\Tenant\Complaint;
use App\Models\Tenant\Specialty;
use App\Models\Tenant\Diagnostic;
use App\Models\Tenant\Appointment;
use App\Models\Tenant\Consultation;
use App\Policies\Tenant\DrugPolicy;
use App\Policies\Tenant\UserPolicy;
use App\Models\Tenant\Investigation;
use App\Models\Tenant\TreatmentPlan;
use App\Policies\Tenant\HistoryPolicy;
use App\Policies\Tenant\PatientPolicy;
use App\Policies\Tenant\SchedulePolicy;
use App\Policies\Tenant\ComplaintPolicy;
use App\Policies\Tenant\SpecialtyPolicy;
use App\Models\Tenant\TreatmentProcedure;
use App\Policies\Tenant\DiagnosticPolicy;
use App\Policies\Tenant\AppointmentPolicy;
use App\Policies\Tenant\ConsultationPolicy;
use App\Policies\Tenant\InvestigationPolicy;
use App\Policies\Tenant\TreatmentPlanPolicy;
use App\Policies\Tenant\TreatmentProcedurePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Patient::class            => PatientPolicy::class,
        Appointment::class        => AppointmentPolicy::class,
        Complaint::class          => ComplaintPolicy::class,
        Consultation::class       => ConsultationPolicy::class,
        Diagnostic::class         => DiagnosticPolicy::class,
        Drug::class               => DrugPolicy::class,
        History::class            => HistoryPolicy::class,
        Investigation::class      => InvestigationPolicy::class,
        Schedule::class           => SchedulePolicy::class,
        Specialty::class          => SpecialtyPolicy::class,
        TreatmentPlan::class      => TreatmentPlanPolicy::class,
        TreatmentProcedure::class => TreatmentProcedurePolicy::class,
        User::class               => UserPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
