<?php

namespace App\Repositories;

interface BaseInterface {

    public function getAll($data);
    public function create(array $data);
    public function getById($id);
    public function destroy($id);
    public function UpdateById($id, array $data);
}
