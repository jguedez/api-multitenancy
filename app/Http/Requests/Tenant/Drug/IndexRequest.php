<?php

namespace App\Http\Requests\Tenant\Drug;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\DrugPolicy;

class IndexRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new DrugPolicy(request()->header('x-tenant'), 'drug.index');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
