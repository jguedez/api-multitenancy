<?php

namespace App\Http\Controllers\Central;

use App\Models\Subscription;
use App\Http\Controllers\Controller;
use App\Http\Requests\Central\User\DestroyRequest;
use App\Repositories\Central\SubscriptionRepository;
use App\Http\Requests\Central\Subscription\ShowRequest;
use App\Http\Requests\Central\Subscription\IndexRequest;
use App\Http\Requests\Central\Subscription\StoreRequest;
use App\Http\Requests\Central\Subscription\UpdateRequest;

class SubscriptionController extends Controller
{
    protected $subscriptionRepository;

    /**
     * @OA\Get(
     *   path="/subscriptions",
     *   tags={"[Central] Subscriptions"},
     *   summary="List all subscriptions filtered by user Authentication",
     *   operationId="IndexInvoice",
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/

    /**
     * Subscription Constructor.
     *
     * @param subscriptionRepository $subscriptionRepository
     */

    public function __construct(SubscriptionRepository $subscriptionRepository)
    {
      $this->subscriptionRepository = $subscriptionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        return response()->json($this->subscriptionRepository->getAll($request->all()));
    }


    /**
     * @OA\Post(
     ** path="/subscriptions",
     *   tags={"[Central] Subscriptions"},
     *   summary="Store new subscriptions",
     *   operationId="StoreSubscriptions",
     *   security={ {"bearer": {}} },
     *
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new Subscriptions",
     *     @OA\JsonContent(
     *          required={"start_at","ends_at","user_id","plan_id"},
     *          @OA\Property(property="start_at", type="string", format="date-time", example="2020-11-30 01:40:54"),
     *          @OA\Property(property="ends_at", type="string", format="date-time", example="2020-12-30 01:40:54"),
     *          @OA\Property(property="user_id", type="integer", example="2"),
     *          @OA\Property(property="plan_id", type="integer", example="2"),
     *    ),
     * ),
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return response()->json($this->subscriptionRepository->create($request->all()))->setStatusCode(201);
    }

    /**
     * @OA\Get(
     *   path="/subscriptions/{id}",
     *   tags={"[Central] Subscriptions"},
     *   summary="Details Subscriptiones",
     *   operationId="ShowSubscriptions",
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, $id)
    {
        return response()->json($this->subscriptionRepository->getById($id));
    }

        /**
     * @OA\Put(
     ** path="/subscriptions/{id}",
     *   tags={"[Central] Subscriptions"},
     *   summary="Update subscriptions",
     *   operationId="UpdateSubscriptions",
     *   security={ {"bearer": {}} },
     *
     *  @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new Subscriptions",
     *     @OA\JsonContent(
     *          required={"start_at","ends_at","user_id","plan_id"},
     *          @OA\Property(property="start_at", type="string", format="date-time", example="2020-11-30 01:40:54"),
     *          @OA\Property(property="ends_at", type="string", format="date-time", example="2020-12-30 01:40:54"),
     *          @OA\Property(property="user_id", type="integer", example="2"),
     *          @OA\Property(property="plan_id", type="integer", example="2"),
     *    ),
     * ),
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->subscriptionRepository->UpdateById($id, $request->all()));
    }

    /**
     * @OA\Delete(
     *   path="/subscriptions/{id}",
     *   tags={"[Central] Subscriptions"},
     *   summary="Delete Subscription",
     *   operationId="DeleteSubscriptions",
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, $id)
    {
        return response()->json($this->subscriptionRepository->destroy($id))->setStatusCode(204);
    }
}
