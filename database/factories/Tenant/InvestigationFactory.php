<?php

namespace Database\Factories\Tenant;

use App\Models\Tenant\Investigation;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvestigationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Investigation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'cod'         => 'LAB-N-'.$this->faker->unique()->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
            'name'        => $this->faker->word,
            'comments'    => $this->faker->sentence(3),
            'created_at'  => now(),
            'updated_at'  => now(),
        ];
    }
}

