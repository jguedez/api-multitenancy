<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;
use App\Repositories\Tenant\TreatmentProcedureRepository;
use App\Http\Requests\Tenant\TreatmentProcedure\ShowRequest;
use App\Http\Requests\Tenant\TreatmentProcedure\IndexRequest;
use App\Http\Requests\Tenant\TreatmentProcedure\StoreRequest;
use App\Http\Requests\Tenant\TreatmentProcedure\UpdateRequest;
use App\Http\Requests\Tenant\TreatmentProcedure\DestroyRequest;

class TreatmentProcedureController extends Controller
{
    protected $treatmentProcedureRepository;

    /**
     * Treatment Plan Constructor.
     *
     * @param TreatmentProcedureRepository $TreatmentProcedureRepository
     */

    public function __construct(TreatmentProcedureRepository $treatmentProcedureRepository)
    {
      $this->treatmentProcedureRepository = $treatmentProcedureRepository;
    }

    /**
     * @OA\Get(
     *   path="/treatment_procedures",
     *   tags={"[Tenant] Treatment Procedures"},
     *   summary="Lists all Treatment Procedures associated with tenant",
     *   operationId="IndexTreatment Procedures",
     *   security={ {"bearer": {}} },
     *
     * @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * )
     **/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        return response()->json($this->treatmentProcedureRepository->getAll($request->all()));
    }

    /**
     * @OA\Post(
     ** path="/treatment_procedures",
     *   tags={"[Tenant] Treatment Procedures"},
     *   summary="Store new Treatment Procedures",
     *   operationId="StoreTreatment Procedures",
     *   security={ {"bearer": {}} },
     *
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new Treatment Procedures",
     *     @OA\JsonContent(
     *          required={"type","name","comments"},
     *          @OA\Property(property="type", type="string", format="string", example="RX"),
     *          @OA\Property(property="name", type="string", format="string", example="Columna Lumbosacro"),
     *          @OA\Property(property="comments", type="string", format="string", example="Lorem ipsu"),
     *    )
     * ),
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
    */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return response()->json($this->treatmentProcedureRepository->create($request->all()))->setStatusCode(201);
    }

    /**
     * @OA\Get(
     *   path="/treatment_procedures/{id}",
     *   tags={"[Tenant] Treatment Procedures"},
     *   summary="Details Treatment Procedures",
     *   operationId="ShowTreatment Procedures",
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, $id)
    {
        return response()->json($this->treatmentProcedureRepository->getById($id));
    }

    /**
     * @OA\Put(
     ** path="/treatment_procedures/{id}",
     *   tags={"[Tenant] Treatment Procedures"},
     *   summary="Update new Treatment Procedures",
     *   operationId="UpdateTreatment Procedures",
     *   security={ {"bearer": {}} },
     *
     *    @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for update new Treatment Procedures",
     *     @OA\JsonContent(
     *          required={"type","name","comments"},
     *          @OA\Property(property="type", type="string", format="string", example="RX"),
     *          @OA\Property(property="name", type="string", format="string", example="Columna Lumbosacro"),
     *          @OA\Property(property="comments", type="string", format="string", example="Lorem ipsu"),
     *    )
     * ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
    */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->treatmentProcedureRepository->UpdateById($id, $request->all()));
    }

    /**
     * @OA\Delete(
     ** path="/treatment_procedures/{id}",
     *   tags={"[Tenant] Treatment Procedures"},
     *   summary="Update new Treatment Procedures",
     *   operationId="UpdateTreatment Procedures",
     *   security={ {"bearer": {}} },
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * )
     **/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, $id)
    {
        return response()->json($this->treatmentProcedureRepository->destroy($id))->setStatusCode(204);
    }
}
