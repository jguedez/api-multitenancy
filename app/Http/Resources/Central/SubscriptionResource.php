<?php

namespace App\Http\Resources\Central;

use Illuminate\Http\Resources\Json\JsonResource;

class SubscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'       => $this->id,
            'start_at' => ($this->start_at)->format('Y-m-d H:m:s'),
            'ends_at'  => ($this->ends_at)->format('Y-m-d H:m:s'),
            'user'     => $this->user,
            'plan'     => $this->plan,
        ];
    }
}
