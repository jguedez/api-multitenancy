<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableHistoryConsultation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_consultation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('history_id');
            $table->unsignedBigInteger('consultation_id');
            $table->timestamps();

            $table->foreign('consultation_id')
            ->references('id')
            ->on('consultations')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('history_id')
            ->references('id')
            ->on('histories')
            ->onUpdate('cascade')
            ->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_consultation');
    }
}
