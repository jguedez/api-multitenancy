<?php

namespace App\Http\Resources\Tenant;

use App\Http\Resources\ApiCollection;
use App\Http\Resources\Tenant\DiagnosticResource;

class DiagnosticCollection extends ApiCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
         return [
            'data'  => DiagnosticResource::collection($this->collection),
            'meta'  => $this->dataMeta($request),
            'links' => $this->dataLinks($request)
        ];
    }
}
