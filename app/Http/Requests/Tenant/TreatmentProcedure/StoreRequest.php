<?php

namespace App\Http\Requests\Tenant\TreatmentProcedure;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\TreatmentProcedurePolicy;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new TreatmentProcedurePolicy(request()->header('x-tenant'), 'treatmentProc.store');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'     => 'required',
            'name'     => 'required',
            'comments' => 'sometimes',
        ];
    }
}
