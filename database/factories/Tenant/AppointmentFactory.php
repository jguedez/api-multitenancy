<?php

namespace Database\Factories\Tenant;

use App\Models\Tenant\Appointment;
use Illuminate\Database\Eloquent\Factories\Factory;

class AppointmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Appointment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'urgency'        => $this->faker->randomElement([true, false]),
            'scheduled_date' => $this->faker->randomElement([now(), now()->addDays(2), now()->addDays(7), now()->addDays(4)]),
            'patient_id'     => $this->faker->numberBetween(1, 50),
            'user_id'        => 1,
        ];
    }
}
