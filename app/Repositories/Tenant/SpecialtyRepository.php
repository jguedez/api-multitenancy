<?php

namespace App\Repositories\Tenant;

use App\Models\Tenant\Specialty;
use App\Repositories\BaseInterface;
use App\Http\Resources\Tenant\SpecialtyResource;
use App\Http\Resources\Tenant\SpecialtyCollection;

class SpecialtyRepository implements BaseInterface
{
    protected $model;
    protected $specialty;

    /**
     * Specialty Repository constructor.
     * @param Drug $specialty
     */
    public function __construct(Specialty $specialty)
    {
        $this->model = $specialty;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new SpecialtyCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new = $this->model::create($data);
        return new SpecialtyResource($new);
    }

    public function getById($id)
    {
        return new SpecialtyResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $this->model::findOrFail($id)->update($data);
        return $this->getById($id);
    }
}
