<?php

namespace App\Models\Tenant;

use App\Models\Tenant\Consultation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Drug extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cod', 'description', 'dosage', 'duration',
        'dosage_form', 'comments'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'pivot'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    /**
     * The consultations that belong to the drugs.
     *
     */
    public function consultations()
    {
        return $this->belongsToMany(Consultation::class);
    }
}
