<?php

namespace App\Repositories\Tenant;

use App\Models\Tenant\History;
use App\Repositories\BaseInterface;
use App\Http\Resources\Tenant\HistoryResource;
use App\Http\Resources\Tenant\HistoryCollection;

class HistoryRepository implements BaseInterface
{
    protected $model;
    protected $history;

    /**
     * History Repository constructor.
     * @param History $history
     */
    public function __construct(History $history)
    {
        $this->model = $history;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new HistoryCollection($this->model::paginate($page));
    }

    public function create(array $data, $key = null)
    {
        $new_history = new History();
        $new_history->fill($data);

        if(empty($data['cod']))
        {
            $new_history->cod = 'HC-'.$key->dni;
        }

        $new_history->save();

        return new HistoryResource($new_history);
    }

    public function getById($id)
    {
        return new HistoryResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $this->model::findOrFail($id)->update($data);
        return $this->getById($id);
    }
}
