<?php

namespace App\Models\Tenant;


use App\Models\Tenant\User;
use App\Models\Tenant\UserSchedule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Schedule extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'starts_at', 'ends_at', 'quotes'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pivot', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    /**
     * The schedules that belong to the user.
     *
     */
    public function users()
    {
        return $this->belongsToMany(User::class)
        ->using(UserSchedule::class);
    }
}
