<?php

namespace App\Repositories\Tenant;

use App\Models\Tenant\Complaint;
use App\Repositories\BaseInterface;
use App\Http\Resources\Tenant\ComplaintResource;
use App\Http\Resources\Tenant\ComplaintCollection;

class ComplaintRepository implements BaseInterface
{
    protected $model;
    protected $complaint;

    /**
     * Complaint Repository constructor.
     * @param Complaint $complaint
     */
    public function __construct(Complaint $complaint)
    {
        $this->model = $complaint;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new ComplaintCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new = $this->model::create($data);
        return new ComplaintResource($new);
    }

    public function getById($id)
    {
        return new ComplaintResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $this->model::findOrFail($id)->update($data);
        return $this->getById($id);
    }
}
