<?php

namespace App\Repositories\Tenant;

use App\Models\Tenant\Appointment;
use App\Repositories\BaseInterface;
use App\Repositories\Tenant\PatientRepository;
use App\Http\Resources\Tenant\AppointmentResource;
use App\Http\Resources\Tenant\AppointmentCollection;

class AppointmentRepository implements BaseInterface
{
    protected $model;
    protected $appointment;

    /**
     * Appointment Repository constructor.
     * @param Appointment $appointment
     */
    public function __construct(Appointment $appointment, PatientRepository $patient)
    {
        $this->model = $appointment;
        $this->patient = $patient;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new AppointmentCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new_appointment = new Appointment();
        $new_appointment->fill($data);

        if(!isset($data['patient_id']))
        {
            $new_patient = $this->patient->create($data);
            $new_appointment->patient_id = $new_patient['id'];

            $new_appointment->save();

            return new AppointmentResource($new_appointment);
        }

        $new_appointment->save();

        return new AppointmentResource($new_appointment);
    }

    public function getById($id)
    {
        return new AppointmentResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $this->model::findOrFail($id)->update($data);
        return $this->getById($id);
    }
}
