<?php

namespace App\Repositories\Tenant;

use App\Repositories\BaseInterface;
use App\Models\Tenant\Investigation;
use App\Http\Resources\Tenant\InvestigationResource;
use App\Http\Resources\Tenant\InvestigationCollection;

class InvestigationRepository implements BaseInterface
{
    protected $model;
    protected $investigation;

    /**
     * Investigation Repository constructor.
     * @param Investigation $investigation
     */
    public function __construct(Investigation $investigation)
    {
        $this->model = $investigation;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new InvestigationCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new = $this->model::create($data);
        return new InvestigationResource($new);
    }

    public function getById($id)
    {
        return new InvestigationResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $this->model::findOrFail($id)->update($data);
        return $this->getById($id);
    }
}
