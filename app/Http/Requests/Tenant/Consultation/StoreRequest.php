<?php

namespace App\Http\Requests\Tenant\Consultation;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\ConsultationPolicy;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new ConsultationPolicy(request()->header('x-tenant'), 'consultation.store');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cod'                  => 'required|unique:consultations,cod',
            'subject'              => 'required',
            'duration'             => 'required',
            'appointment_id'       => 'required|exists:appointments,id|unique:consultations,appointment_id',
            'histories'            => 'required|exists:histories,id',
            'diagnostics'          => 'required|exists:diagnostics,id',
            'complaints'           => 'required|exists:complaints,id',
            'investigations'       => 'required|exists:investigations,id',
            'treatment_plans'      => 'required|exists:treatment_plans,id',
            'treatment_procedures' => 'sometimes|exists:treatment_procedures,id',
            'drugs'                => 'sometimes|exists:drugs,id',
        ];
    }
}
