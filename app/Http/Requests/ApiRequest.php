<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class ApiRequest extends FormRequest
{
     /**
     * @param $resource_class
     * @param $record_id
     * @return mixed
     */
    protected function findRecordInRequest($resource_class, $record_id)
    {
        return call_user_func_array([$resource_class, 'findOrFail'], [$record_id]);
    }
}
