<?php

namespace Database\Seeders\Tenant;

use App\Models\Tenant\Patient;
use Illuminate\Database\Seeder;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Patient::factory()->count(50)->create();
    }
}
