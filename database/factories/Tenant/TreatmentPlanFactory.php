<?php

namespace Database\Factories\Tenant;

use App\Models\Tenant\TreatmentPlan;
use Illuminate\Database\Eloquent\Factories\Factory;

class TreatmentPlanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TreatmentPlan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'plan' => $this->faker->sentence(4),
            'status' => $this->faker->randomElement([true, false]),
            'comments' => $this->faker->sentence(20),
            'created_at'  => now(),
            'updated_at'  => now(),
        ];
    }
}
