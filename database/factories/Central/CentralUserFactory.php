<?php

namespace Database\Factories\Central;

use App\Models\Central\CentralUser;
use Illuminate\Database\Eloquent\Factories\Factory;

class CentralUserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CentralUser::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'global_id'  => $this->faker->word,
            'first_name' => $this->faker->name,
            'last_name'  => $this->faker->lastName,
            'dni' => $this->faker->unique()->ean8,
            'email' => $this->faker->unique()->safeEmail,
            'password' => bcrypt('secret'),
            'is_active' => true,
            'address' => [
                'street'  => $this->faker->address,
                'country' => $this->faker->country,
                'state'   => $this->faker->state,
                'city'    => $this->faker->city
            ],
            'phone'      => [
                'landline' => $this->faker->e164PhoneNumber,
                'movil'    => $this->faker->e164PhoneNumber
            ],
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
