<?php

namespace Database\Seeders\Tenant;

use App\Models\Tenant\History;
use Illuminate\Database\Seeder;

class HistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        History::factory()->count(50)->create();

    }
}
