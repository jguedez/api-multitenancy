<?php

namespace App\Repositories\Tenant;

use App\Repositories\BaseInterface;
use App\Models\Tenant\TreatmentProcedure;
use App\Http\Resources\Tenant\TreatmentProcedureResource;
use App\Http\Resources\Tenant\TreatmentProcedureCollection;

class TreatmentProcedureRepository implements BaseInterface
{
    protected $model;
    protected $treatmentProcedure;

    /**
     * Treatment Procedure Repository constructor.
     * @param TreatmentProcedure $treatmentProcedure
     */
    public function __construct(TreatmentProcedure $treatmentProcedure)
    {
        $this->model = $treatmentProcedure;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new TreatmentProcedureCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new = $this->model::create($data);
        return new TreatmentProcedureResource($new);
    }

    public function getById($id)
    {
        return new TreatmentProcedureResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $this->model::findOrFail($id)->update($data);
        return $this->getById($id);
    }

}
