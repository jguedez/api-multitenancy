<?php

namespace App\Models\Tenant;

use App\Models\Tenant\User;
use App\Models\Tenant\Patient;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Person extends Model
{
    use HasFactory;

           /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'age', 'dni',
        'cuit', 'nationality', 'picture', 'gender',
        'birthday', 'marital_status', 'phone', 'address',
        'blood_type', 'race'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'phone'              => 'array',
        'address'            => 'array',
        'birthday'           => 'date:Y-m-d'
    ];

    /**
     * Get full name the person
     * @return string
     */
    public function getFullNameAttribute(){
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * Get the patient that owns the person.
     *
     */
    public function patient()
    {
        return $this->belongsTo(Patient::class, 'person_id');
    }

    /**
     * Get the user that owns the person.
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'person_id');
    }
}
