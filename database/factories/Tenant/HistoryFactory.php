<?php

namespace Database\Factories\Tenant;

use App\Models\Tenant\History;
use Illuminate\Database\Eloquent\Factories\Factory;

class HistoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = History::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'cod'              => 'HC-'.$this->faker->unique()->ean8,
            'allergies'        => $this->faker->sentence(10),
            'medication'       => $this->faker->sentence(6),
            'surgical_history' => $this->faker->sentence(4),
            'risk_factors'     => $this->faker->sentence(1),
            'toxic_habits'     => $this->faker->sentence(6)
        ];
    }
}
