<?php

namespace App\Http\Requests\Tenant\History;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\HistoryPolicy;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new HistoryPolicy(request()->header('x-tenant'), 'history.store');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cod'              => 'required|unique:histories,cod',
            'allergies'        => 'sometimes',
            'medication'       => 'sometimes',
            'surgical_history' => 'sometimes',
            'risk_factors'     => 'sometimes',
            'toxic_habits'     => 'sometimes'
        ];
    }
}
