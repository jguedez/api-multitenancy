<?php

namespace Database\Factories\Central;

use App\Models\Central\Invoice;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Invoice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'subject'          => 'Bill subscriptions for plan',
            'id_payment'       => $this->faker->unique->creditCardNumber,
            'due_date'         => now(),
            'excise_duty'      => $this->faker->randomFloat(2, 100, 300),
            'tax'              => $this->faker->randomFloat(2, 100, 300),
            'currency'         => 'USD',
            'details'          => [
                'article'     => $this->faker->word,
                'description' => $this->faker->sentence(6),
                'amount'      => $this->faker->randomFloat(2, 100, 300)
            ],
            'total'            => $this->faker->randomFloat(2, 100, 300),
            'shipping_address' => [
                'street'  => $this->faker->address,
                'country' => $this->faker->country,
                'state'   => $this->faker->state,
                'city'    => $this->faker->city
            ],
            'subscription_id'     => $this->faker->unique()->randomElement([1, 2, 3]),
            'created_at'       => now(),
            'updated_at'       => now(),
        ];
    }
}
