<?php

namespace App\Http\Resources\Central;

use App\Http\Resources\ApiCollection;
use App\Http\Resources\Central\SubscriptionResource;

class SubscriptionCollection extends ApiCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data'  => SubscriptionResource::collection($this->collection),
            'meta'  => $this->dataMeta($request),
            'links' => $this->dataLinks($request)
        ];
    }
}
