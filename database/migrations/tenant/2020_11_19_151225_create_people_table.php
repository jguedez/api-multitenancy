<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('age')->nullable();
            $table->string('dni')->unique();
            $table->string('cuit')->nullable();
            $table->string('nationality')->nullable();
            $table->string('picture')->nullable();
            $table->string('gender')->nullable();
            $table->dateTime('birthday', 0)->nullable();
            $table->string('marital_status')->nullable();
            $table->json('phone')->nullable();
            $table->json('address')->nullable();
            $table->string('blood_type')->nullable();
            $table->string('race')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
