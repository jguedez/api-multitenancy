<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelationshipDatabaseCentral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subcriptions', function (Blueprint $table) {
            $table->foreignId('user_id')
            ->constrained('users')
            ->nullable()
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreignId('plan_id')
            ->constrained('plans')
            ->nullable()
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });

        Schema::table('invoices', function (Blueprint $table) {
            $table->foreignId('subscription_id')
            ->constrained('subcriptions')
            ->nullable()
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subcriptions', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['plan_id']);
        });

        Schema::table('invoices', function (Blueprint $table) {
            $table->dropForeign(['subscription_id']);
        });
    }
}
