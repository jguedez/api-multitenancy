<?php

namespace App\Http\Requests\Tenant\Appointment;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\AppointmentPolicy;

class DestroyRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new AppointmentPolicy(request()->header('x-tenant'), 'appointment.destroy');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
