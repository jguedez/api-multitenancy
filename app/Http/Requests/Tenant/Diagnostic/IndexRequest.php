<?php

namespace App\Http\Requests\Tenant\Diagnostic;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\DiagnosticPolicy;

class IndexRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new DiagnosticPolicy(request()->header('x-tenant'), 'diagnostic.index');
        return $policy->check();    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
