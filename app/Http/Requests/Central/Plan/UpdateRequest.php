<?php

namespace App\Http\Requests\Central\Plan;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $plan_id = $this->route()->parameters('plans');

        return [
            'name'          => 'required|unique:plans,name,'.$plan_id['plan'],
            'description'   => 'sometimes',
            'currency'      => 'required',
            'benefits'      => 'required',
            'unit_price'    => 'required|numeric',
            'time_duration' => 'required|numeric',
        ];
    }
}
