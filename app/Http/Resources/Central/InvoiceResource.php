<?php

namespace App\Http\Resources\Central;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Central\SubscriptionCollection;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'subject'          => $this->subject,
            'id_payment'       => $this->id_payment,
            'due_date'         => $this->due_date,
            'excise_duty'      => $this->excise_duty,
            'tax'              => $this->tax,
            'currency'         => $this->currency,
            'details'          => $this->details,
            'total'            => $this->total,
            'shipping_address' => $this->shipping_address,
            'subscriptions'    => $this->subscription->generic,
            'created_at'       => ($this->created_at)->format('Y-m-d H:m:s'),
            'updated_at'       => ($this->created_at)->format('Y-m-d H:m:s')
        ];
    }
}
