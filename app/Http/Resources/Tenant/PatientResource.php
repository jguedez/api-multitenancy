<?php

namespace App\Http\Resources\Tenant;

use Illuminate\Http\Resources\Json\JsonResource;

class PatientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'person'            => $this->people,
            'occupation'        => $this->occupation,
            'guardian'          => $this->guardian,
            'emergency_contact' => $this->emergency_contact,
            'status'            => $this->status,
            'social_security'   => $this->social_security,
            'history'           => $this->histories,
            'created_at'        => ($this->created_at)->format('Y-m-d H:m:s'),
            'updated_at'        => ($this->updated_at)->format('Y-m-d H:m:s'),
        ];
    }
}
