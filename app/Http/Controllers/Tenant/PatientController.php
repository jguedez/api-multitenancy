<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;
use App\Repositories\Tenant\PatientRepository;
use App\Http\Requests\Tenant\Patient\ShowRequest;
use App\Http\Requests\Tenant\Patient\IndexRequest;
use App\Http\Requests\Tenant\Patient\StoreRequest;
use App\Http\Requests\Tenant\Patient\UpdateRequest;
use App\Http\Requests\Tenant\Patient\DestroyRequest;

class PatientController extends Controller
{
    protected $patientRepository;

    /**
     * Drug Constructor.
     *
     * @param PatientRepository $patientRepository
     */

    public function __construct(PatientRepository $patientRepository)
    {
      $this->patientRepository = $patientRepository;
    }

    /**
     * @OA\Get(
     *   path="/patients",
     *   tags={"[Tenant] Patients"},
     *   summary="Lists all Patients associated with tenant",
     *   operationId="IndexPatients",
     *   security={ {"bearer": {}} },
     *
     * @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * )
     **/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        return response()->json($this->patientRepository->getAll($request->all()));
    }

    /**
     * @OA\Post(
     ** path="/patients",
     *   tags={"[Tenant] Patients"},
     *   summary="Store new Patients",
     *   operationId="StorePatients",
     *   security={ {"bearer": {}} },
     *
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new Patients",
     *     @OA\JsonContent(
     *          required={"first_name","last_name","age","dni","gender","marital_status","phone","address","occupation","emergency_contact","status"},
     *          @OA\Property(property="first_name", type="string", format="string", example="Erika"),
     *          @OA\Property(property="last_name", type="string", format="string", example="Rodriguez"),
     *          @OA\Property(property="age", type="integer", format="integer", example="21"),
     *          @OA\Property(property="dni", type="string", format="string", example="2438294839"),
     *          @OA\Property(property="cuit", type="string", format="string", example="23423423894"),
     *          @OA\Property(property="nationality", type="string", format="string", example="Venezolana"),
     *          @OA\Property(property="picture", type="string", format="url", example="https://via.placeholder.com/150x200.png/00ee44?text=people+similique"),
     *          @OA\Property(property="gender", type="string", format="string", example="F"),
     *          @OA\Property(property="birthday", type="string", format="date", example="25-01-1999"),
     *          @OA\Property(property="marital_status", type="string", format="string", example="Soltera"),
     *          @OA\Property(property="phone", type="object", format="string", example={"landline":"+3636833483345","movil":"+7794203195864"}),
     *          @OA\Property(property="address", type="object", format="string", example={"street":"3806 Murphy Burgs Suite 119\nSouth Suzannefort, MO 01668","country":"Mongolia","state":"Maryland","city":"Port Vidabury"}),
     *          @OA\Property(property="blood_type", type="string", format="string", example="AB+"),
     *          @OA\Property(property="race", type="string", format="string", example="Pura"),
     *          @OA\Property(property="occupation", type="string", format="string", example="Estudiante"),
     *          @OA\Property(property="guardian", type="object", format="string", example={"name":"Jazmyn Fisher","phone":"+18322291680","relatinship":"family"}),
     *          @OA\Property(property="emergency_contact", type="object", format="string", example="empty"),
     *          @OA\Property(property="social_security", type="string", format="string", example="Pre-Operatoria"),
     *          @OA\Property(property="cod", type="string", format="string", example="HC-0340304"),
     *          @OA\Property(property="allergies", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="medication", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="surgical_history", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="risk_factors", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="toxic_habits", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="person_id", type="integer", format="integer", example=1),
     *          @OA\Property(property="history_id", type="integer", format="integer", example=5),
     *    )
     * ),
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
    */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return response()->json($this->patientRepository->create($request->all()))->setStatusCode(201);
    }

    /**
     * @OA\Get(
     *   path="/patients/{id}",
     *   tags={"[Tenant] Patients"},
     *   summary="Details Patients",
     *   operationId="ShowPatients",
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, $id)
    {
        return response()->json($this->patientRepository->getById($id));
    }

    /**
     * @OA\Put(
     ** path="/patients/{id}",
     *   tags={"[Tenant] Patients"},
     *   summary="Update new Patients",
     *   operationId="UpdatePatients",
     *   security={ {"bearer": {}} },
     *
     *    @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for update new Patients",
     *     @OA\JsonContent(
     *          required={"first_name","last_name","age","dni","gender","marital_status","occupation","emergency_contact","status"},
     *          @OA\Property(property="first_name", type="string", format="string", example="Erika"),
     *          @OA\Property(property="last_name", type="string", format="string", example="Rodriguez"),
     *          @OA\Property(property="age", type="integer", format="integer", example="21"),
     *          @OA\Property(property="dni", type="string", format="string", example="2438294839"),
     *          @OA\Property(property="cuit", type="string", format="string", example="23423423894"),
     *          @OA\Property(property="nationality", type="string", format="string", example="Venezolana"),
     *          @OA\Property(property="picture", type="string", format="url", example="https://via.placeholder.com/150x200.png/00ee44?text=people+similique"),
     *          @OA\Property(property="gender", type="string", format="string", example="F"),
     *          @OA\Property(property="birthday", type="string", format="date", example="25-01-1999"),
     *          @OA\Property(property="marital_status", type="string", format="string", example="Soltera"),
     *          @OA\Property(property="phone", type="object", format="string", example={"landline":"+3636833483345","movil":"+7794203195864"}),
     *          @OA\Property(property="address", type="object", format="string", example={"street":"3806 Murphy Burgs Suite 119\nSouth Suzannefort, MO 01668","country":"Mongolia","state":"Maryland","city":"Port Vidabury"}),
     *          @OA\Property(property="blood_type", type="string", format="string", example="AB+"),
     *          @OA\Property(property="race", type="string", format="string", example="Pura"),
     *          @OA\Property(property="occupation", type="string", format="string", example="Estudiante"),
     *          @OA\Property(property="guardian", type="object", format="string", example={"name":"Jazmyn Fisher","phone":"+18322291680","relatinship":"family"}),
     *          @OA\Property(property="emergency_contact", type="object", format="string", example="empty"),
     *          @OA\Property(property="social_security", type="string", format="string", example="Pre-Operatoria"),
     *          @OA\Property(property="cod", type="string", format="string", example="HC-0340304"),
     *          @OA\Property(property="allergies", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="medication", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="surgical_history", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="risk_factors", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="toxic_habits", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="person_id", type="integer", format="integer", example=1),
     *          @OA\Property(property="history_id", type="integer", format="integer", example=5),
     *    )
     * ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
    */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->patientRepository->UpdateById($id, $request->all()));
    }

    /**
     * @OA\Delete(
     ** path="/patients/{id}",
     *   tags={"[Tenant] Patients"},
     *   summary="Update new Patients",
     *   operationId="UpdatePatients",
     *   security={ {"bearer": {}} },
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * )
     **/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, $id)
    {
        return response()->json($this->patientRepository->destroy($id))->setStatusCode(204);
    }
}
