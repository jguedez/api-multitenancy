<?php

namespace App\Http\Requests\Tenant\Complaint;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\ComplaintPolicy;

class IndexRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new ComplaintPolicy(request()->header('x-tenant'), 'complaint.index');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
