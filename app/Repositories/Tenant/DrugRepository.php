<?php

namespace App\Repositories\Tenant;

use App\Models\Tenant\Drug;
use App\Repositories\BaseInterface;
use App\Http\Resources\Tenant\DrugResource;
use App\Http\Resources\Tenant\DrugCollection;

class DrugRepository implements BaseInterface
{
    protected $model;
    protected $drug;

    /**
     * Drug Repository constructor.
     * @param Drug $drug
     */
    public function __construct(Drug $drug)
    {
        $this->model = $drug;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new DrugCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new = $this->model::create($data);
        return new DrugResource($new);
    }

    public function getById($id)
    {
        return new DrugResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $this->model::findOrFail($id)->update($data);
        return $this->getById($id);
    }
}
