<?php

namespace App\Http\Resources\Tenant;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'global_id'    => $this->global_id,
            'first_name'   => $this->first_name,
            'last_name'    => $this->last_name,
            'dni'          => $this->dni,
            'email'        => $this->email,
            'is_active'    => $this->is_active,
            'last_login'   => $this->last_login,
            'address'      => $this->address,
            'phone'        => $this->phone,
            'specialties'  => $this->specialties,
            'schedules'    => $this->schedules,
            'created_at'   => ($this->created_at)->format('Y-m-d H:m:s'),
            'updated_at'   => ($this->updated_at)->format('Y-m-d H:m:s'),
        ];
    }
}
