# Multitenancy - API. 

### Pre-requisitos 📋

_Antes de comenzar, necesitas los siguiente requerimientos minimos instalados en tu equipo para poder correr el proyecto_

```
MYSQL 5.7
Composer
```

### Instalación 🔧

_Clona el repositorio y ejecuta los siguientes comandos en el interior de la carpeta del proyecto._

```
Composer install
cp .env.example .env 
php artisan key:generate 
php artisan jwt:secret
php artisan migrate --seed 
php artisan tenants:seed
php artisan serve
```

><b>NOTA</b>: Recuerda haber colocado las credenciales de tu BD en el .env antes de correr las migraciones.  

_Y en caso de que ya tengas las migraciones ejecutadas y solo quieres incluir las nuevas_

```
php artisan migrate:refresh --seed [Base de datos Central]
php artisan tenants:migrate-fresh && php artisan tenants:seed [Para los inquilinos]
```

><b>NOTA</b>: El `seed` solo es en caso de que quieras generar la data dump para efectos de pruebas. Una vez en produccion no es recomendable usar esto. Actualmente no se encuentra disponible la opcion de borrar la base de datos inquilina al hacer el refrescado por lo cual no esta demas recordar eliminar el anterior esquema de forma manual para no dejar esquemas basura. 

## Modulos Disponibles ⚙

## (Usuario Central)
* User (CentralUser)
* Subcriptions
* Invoices
* Plans
* Tenant

## (Usuario Inquilino)
* User (Doctors)
* Appointments
* Schedules
* Consultations
* Day
* Diagnostics
* Drugs
* Investigations
* Complaints
* Treatment Procedures
* Treatment Plan
* Histories (Clinic)
* Specialties
* Patients
* People

_Pendiente anexar documentacion_

## Despliegue 📦

_En construccion_

## Licencia 📄

_Pendiente especificar Licencia_
