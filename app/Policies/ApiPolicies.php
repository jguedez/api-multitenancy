<?php

namespace App\Policies;

use App\Models\Tenant\User;
use Spatie\Permission\Models\Role;

abstract class ApiPolicies
{
    protected function getRoleUserTenant($x_tenant)
    {
        $user = User::where('global_id', $x_tenant)->first();

        return $user->getRoleNames();
    }

    protected function checkPermission($x_tenant, $permission)
    {
        $role = $this->getRoleUserTenant($x_tenant);
        $verify = array();

        for($i = 0; $i < count($role); $i++)
        {
           $role_own[$i] =  Role::findByName($role[$i]);
           $verify[$i] = $role_own[$i]->hasPermissionTo($permission);
        }

        if (in_array(true, $verify))
        {
            return true;
        }

        return false;
    }

}
