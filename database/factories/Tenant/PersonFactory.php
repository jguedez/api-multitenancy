<?php

namespace Database\Factories\Tenant;

use App\Models\Tenant\Person;
use Illuminate\Database\Eloquent\Factories\Factory;

class PersonFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Person::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name'     => $this->faker->name,
            'last_name'      => $this->faker->lastName,
            'age'            => $this->faker->randomNumber(2, false),
            'dni'            => $this->faker->unique()->ean8,
            'cuit'           => $this->faker->unique()->ean8,
            'nationality'    => $this->faker->country,
            'picture'        => $this->faker->imageUrl(150, 200, 'people'),
            'gender'         => $this->faker->randomElement(['female', 'male', 'unknow']),
            'birthday'       => $this->faker->date('Y-m-d'),
            'marital_status' => $this->faker->randomElement(['Single', 'Married']),
            'phone'          => $this->faker->phoneNumber,
            'address' => [
                'street'  => $this->faker->address,
                'country' => $this->faker->country,
                'state'   => $this->faker->state,
                'city'    => $this->faker->city
            ],
            'phone'      => [
                'landline' => $this->faker->e164PhoneNumber,
                'movil'    => $this->faker->e164PhoneNumber
            ],
            'blood_type'   => $this->faker->randomElement(['O', 'A-', 'A', 'B-', 'B', 'AB', 'AB-', 'O-']),
            'race'         => $this->faker->word,
            'created_at'   => now(),
            'updated_at'   => now()
        ];
    }
}

