<?php

namespace App\Http\Requests\Tenant\Patient;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\PatientPolicy;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new PatientPolicy(request()->header('x-tenant'), 'patient.store');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'        => 'required',
            'last_name'         => 'required',
            'age'               => 'required',
            'dni'               => 'required_without:person_id|unique:people,dni',
            'cuit'              => 'sometimes|unique:people,cuit',
            'nationality'       => 'sometimes',
            'picture'           => 'nullable',
            'gender'            => 'required',
            'birthday'          => 'sometimes',
            'marital_status'    => 'required',
            'phone'             => 'sometimes',
            'phone.landline'    => 'required_with:phone',
            'phone.movil'       => 'required_with:phone',
            'address'           => 'sometimes',
            'address.street'    => 'required_with:address',
            'address.country'   => 'required_with:address',
            'address.state'     => 'required_with:address',
            'address.city'      => 'required_with:address',
            'blood_type'        => 'sometimes',
            'race'              => 'sometimes',
            'occupation'        => 'required',
            'guardian'          => 'sometimes',
            'emergency_contact' => 'required',
            'status'            => 'required',
            'social_security'   => 'sometimes',
            'cod'               => 'required_without:history_id',
            'allergies'         => 'required_without:history_id',
            'medication'        => 'required_without:history_id',
            'surgical_history'  => 'required_without:history_id',
            'risk_factors'      => 'required_without:history_id',
            'toxic_habits'      => 'required_without:history_id',
            'person_id'         => 'required_without:dni|exists:people,id|unique:people,id',
            'history_id'        => 'required_without:person_id|exists:histories,id'
        ];
    }
}
