<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;
use App\Repositories\Tenant\DrugRepository;
use App\Http\Requests\Tenant\Drug\ShowRequest;
use App\Http\Requests\Tenant\Drug\IndexRequest;
use App\Http\Requests\Tenant\Drug\StoreRequest;
use App\Http\Requests\Tenant\Drug\UpdateRequest;
use App\Http\Requests\Tenant\Drug\DestroyRequest;

class DrugController extends Controller
{
    protected $drugRepository;

    /**
     * Drug Constructor.
     *
     * @param DrugRepository $drugRepository
     */

    public function __construct(DrugRepository $drugRepository)
    {
      $this->drugRepository = $drugRepository;
    }

    /**
     * @OA\Get(
     *   path="/drugs",
     *   tags={"[Tenant] Drugs"},
     *   summary="Lists all Drugs associated with tenant",
     *   operationId="IndexDrugs",
     *   security={ {"bearer": {}} },
     *
     * @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * )
     **/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        return response()->json($this->drugRepository->getAll($request->all()));
    }

    /**
     * @OA\Post(
     ** path="/drugs",
     *   tags={"[Tenant] Drugs"},
     *   summary="Store new Drugs",
     *   operationId="StoreDrugs",
     *   security={ {"bearer": {}} },
     *
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new Drugs",
     *     @OA\JsonContent(
     *          required={"cod","dosage","duration","dosage_form"},
     *          @OA\Property(property="cod", type="string", format="string", example="DRUG-00001"),
     *          @OA\Property(property="description", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="dosage", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="duration", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="dosage_form", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="comments", type="string", format="string", example="Lorem ipsu"),

     *    )
     * ),
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
    */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return response()->json($this->drugRepository->create($request->all()))->setStatusCode(201);
    }

    /**
     * @OA\Get(
     *   path="/drugs/{id}",
     *   tags={"[Tenant] Drugs"},
     *   summary="Details Drugs",
     *   operationId="ShowDrugs",
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, $id)
    {
        return response()->json($this->drugRepository->getById($id));
    }

    /**
     * @OA\Put(
     ** path="/drugs/{id}",
     *   tags={"[Tenant] Drugs"},
     *   summary="Update new Drugs",
     *   operationId="UpdateDrugs",
     *   security={ {"bearer": {}} },
     *
     *    @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for update new Drugs",
     *     @OA\JsonContent(
    *          required={"cod","dosage","duration","dosage_form"},
     *          @OA\Property(property="cod", type="string", format="string", example="DRUG-00001"),
     *          @OA\Property(property="description", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="dosage", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="duration", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="dosage_form", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="comments", type="string", format="string", example="Lorem ipsu"),
     *    )
     * ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
    */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->drugRepository->UpdateById($id, $request->all()));
    }

    /**
     * @OA\Delete(
     ** path="/drugs/{id}",
     *   tags={"[Tenant] Drugs"},
     *   summary="Update new Drugs",
     *   operationId="UpdateDrugs",
     *   security={ {"bearer": {}} },
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * )
     **/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, $id)
    {
        return response()->json($this->drugRepository->destroy($id))->setStatusCode(204);
    }
}
