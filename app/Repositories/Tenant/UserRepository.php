<?php

namespace App\Repositories\Tenant;

use App\Models\Tenant\User;
use App\Repositories\BaseInterface;
use App\Http\Resources\Tenant\UserResource;
use App\Http\Resources\Tenant\UserCollection;

class UserRepository implements BaseInterface
{
    protected $model;
    protected $user;

    /**
     * User Repository constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new UserCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $user = new User();
        $user->fill($data);
        $user->password = bcrypt('password');
        $user->is_active = true;
        $user->save();

        $format = $this->formatSync($data['schedules']['schedule'], $data['schedules']['day']);
        $user->schedules()->sync($format);
        $user->specialties()->sync($data['specialties']);

        return new UserResource($user);

    }
    public function getById($id)
    {
        return new UserResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $user = $this->getById($id);
        $user->fill($data);

        if (isset($data['password']))
        {
            $user['password'] = bcrypt($data['password']);
        }

        $user->save();

        $format = $this->formatSync($data['schedules']['schedule'], $data['schedules']['day']);
        $user->schedules()->sync($format);
        $user->specialties()->sync($data['specialties']);

        return $this->getById($id);
    }

    protected function formatSync($schedules_id, $days_id)
    {
        $extra = array_map(function($days_id){
            return ['day_id' => $days_id];
        }, $schedules_id);

        return array_combine($schedules_id, $extra);
    }
}
