<?php

namespace Database\Factories\Tenant;

use App\Models\Tenant\Consultation;
use Illuminate\Database\Eloquent\Factories\Factory;

class ConsultationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Consultation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'cod'            => 'CN-'.$this->faker->unique()->ean8,
            'subject'        => $this->faker->word,
            'duration'       => now()->format('H:m:s'),
            'appointment_id' => $this->faker->unique()->numberBetween(1, 10)

        ];
    }
}
