<?php

namespace App\Models\Tenant;


use App\Models\Tenant\Drug;
use App\Models\Tenant\History;
use App\Models\Tenant\Complaint;
use App\Models\Tenant\Diagnostic;
use App\Models\Tenant\Appointment;
use App\Models\Tenant\Investigation;
use App\Models\Tenant\TreatmentPlan;
use Illuminate\Database\Eloquent\Model;
use App\Models\Tenant\TreatmentProcedure;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Consultation extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cod', 'subject', 'duration', 'appointment_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];


    /**
     * The complaits that belong to the consult.
     *
     */
    public function complaits()
    {
        return $this->belongsToMany(Complaint::class,
            'consultation_complaints',
            'consultation_id',
            'complaint_id'
        );
    }

     /**
     * The diagnostics that belong to the consult.
     *
     */
    public function diagnostics()
    {
        return $this->belongsToMany(Diagnostic::class,
            'consultation_diagnostic',
            'consultation_id',
            'diagnostic_id'
        );
    }

    /**
     * The drugs that belong to the consult.
     *
     */
    public function drugs()
    {
        return $this->belongsToMany(Drug::class,
            'consultation_drug',
            'consultation_id',
            'drug_id'
        );
    }

    /**
     * The treatment plan that belong to the consult.
     *
     */
    public function treatmentPlan()
    {
        return $this->belongsToMany(TreatmentPlan::class,
            'consultation_treatment_plan',
            'consultation_id',
            'treatment_plan_id'
        );
    }

    /**
     * The treatment procedure that belong to the consult.
     *
     */
    public function treatmentProcedure()
    {
        return $this->belongsToMany(TreatmentProcedure::class,
            'consultation_treatment_procedure',
            'consultation_id',
            'treatment_procedure_id'
        );
    }

    /**
     * The investigation that belong to the consult.
     *
     */
    public function investigation()
    {
        return $this->belongsToMany(Investigation::class,
            'consultation_investigation',
            'consultation_id',
            'investigation_id'
        );
    }

    /**
     * The history that belong to the consultation.
     *
     */
    public function histories()
    {
        return $this->belongsToMany(History::class,
        'history_consultation',
        'consultation_id',
        'history_id');
    }

    /**
     * Get the appointment for the  clinic consultation
     *
     */
    public function appointments()
    {
        return $this->hasOne(Appointment::class,
            'id',
            'appointment_id'
        );
    }
}

