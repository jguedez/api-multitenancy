<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableConsultationTreatmentProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultation_treatment_procedure', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('consultation_id');
            $table->unsignedBigInteger('treatment_procedure_id');
            $table->timestamps();

            $table->foreign('consultation_id')
            ->references('id')
            ->on('consultations')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('treatment_procedure_id')
            ->references('id')
            ->on('treatment_procedures')
            ->onUpdate('cascade')
            ->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultation_treatment_procedure');
    }
}
