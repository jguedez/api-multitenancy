<?php

namespace App\Http\Controllers\Central;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Central\InvoiceRepository;
use App\Http\Requests\Central\Invoice\ShowRequest;
use App\Http\Requests\Central\Invoice\IndexRequest;
use App\Http\Requests\Central\Invoice\StoreRequest;
use App\Http\Requests\Central\Invoice\UpdateRequest;
use App\Http\Requests\Central\Invoice\DestroyRequest;

class InvoiceController extends Controller
{
    protected $invoiceRepository;

    /**
     * Plan Constructor.
     *
     * @param invoiceRepository $invoiceRepository
     */

    public function __construct(InvoiceRepository $invoiceRepository)
    {
      $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * @OA\Get(
     *   path="/invoices",
     *   tags={"[Central] Invoices"},
     *   summary="List all Invoices filtered by user Authentication",
     *   operationId="IndexInvoice",
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        return response()->json($this->invoiceRepository->getAll($request->all()));
    }

    /**
     * @OA\Post(
     ** path="/invoices",
     *   tags={"[Central] Invoices"},
     *   summary="Store new Invoice",
     *   operationId="StoreInvoice",
     *   security={ {"bearer": {}} },
     *
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new Invoice",
     *     @OA\JsonContent(
     *          required={"id_payment","due_date","excise_duty","tax","currency","details", "total", "subscription_id"},
     *          @OA\Property(property="id_payment", type="string", format="string", example="P324fdfjdkf"),
     *          @OA\Property(property="due_date", type="string", format="string", example="2021-01-02"),
     *          @OA\Property(property="tax", type="integer", example="235"),
     *          @OA\Property(property="details", type="object", example="[{'article':'plan X', 'description': 'lorempipsu', 'amount': '343.44'}]"),
     *          @OA\Property(property="total", type="integer", example="253"),
     *          @OA\Property(property="subscription_id", type="integer", example="3"),
     *    ),
     * ),
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                       "id_payment": "P324fdfjdkf",
     *                       "due_date": "2021-01-02",
     *                       "tax": "235",
     *                       "details": "[{'article':'plan X', 'description': 'lorempipsu', 'amount': '343.44'}]",
     *                       "total": "253",
     *                       "subscription_id": "3"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return response()->json($this->invoiceRepository->create($request->all()))->setStatusCode(201);
    }

    /**
     * @OA\Get(
     *   path="/invoices/{id}",
     *   tags={"[Central] Invoices"},
     *   summary="Details invoice",
     *   operationId="ShowInvoice",
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, $id)
    {
        return response()->json($this->invoiceRepository->getById($id));
    }

    /**
     * @OA\Put(
     ** path="/invoices",
     *   tags={"[Central] Invoices"},
     *   summary="Store new Invoice",
     *   operationId="StoreInvoice",
     *   security={ {"bearer": {}} },
     *
     *   @OA\Parameter(
     *        name="id",
     *        in="path",
     *        required=true,
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new Invoice",
     *     @OA\JsonContent(
     *          required={"id_payment","currency","details", "total", "subscription_id"},
     *          @OA\Property(property="id_payment", type="string", format="string", example="P324fdfjdkf"),
     *          @OA\Property(property="due_date", type="string", format="string", example="2021-01-02"),
     *          @OA\Property(property="tax", type="integer", example="235"),
     *          @OA\Property(property="details", type="object", example="[{'article':'plan X', 'description': 'lorempipsu', 'amount': '343.44'}]"),
     *          @OA\Property(property="total", type="integer", example="253"),
     *          @OA\Property(property="subscription_id", type="integer", example="3"),
     *    ),
     * ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                       "id_payment": "P324fdfjdkf",
     *                       "due_date": "2021-01-02",
     *                       "tax": "235",
     *                       "details": "[{'article':'plan X', 'description': 'lorempipsu', 'amount': '343.44'}]",
     *                       "total": "253",
     *                       "subscription_id": "3"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->invoiceRepository->UpdateById($id, $request->all()));
    }

    /**
     * @OA\Delete(
     *   path="/invoices/{id}",
     *   tags={"[Central] Invoices"},
     *   summary="Delete Invoice",
     *   operationId="DeleteInvoice",
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, $id)
    {
        return response()->json($this->invoiceRepository->destroy($id))->setStatusCode(204);
    }
}
