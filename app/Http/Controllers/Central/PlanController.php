<?php

namespace App\Http\Controllers\Central;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Central\PlanRepository;
use App\Http\Requests\Central\Plan\ShowRequest;
use App\Http\Requests\Central\Plan\IndexRequest;
use App\Http\Requests\Central\Plan\UpdateRequest;
use App\Http\Requests\Central\Plan\DestroyRequest;
use App\Http\Requests\Central\Plan\StoreRequest;

class PlanController extends Controller
{
    protected $planRepository;

    /**
     * Plan Constructor.
     *
     * @param planRepository $planRepository
     */

    public function __construct(PlanRepository $planRepository)
    {
      $this->planRepository = $planRepository;
    }

    /**
     * @OA\Get(
     *   path="/plans",
     *   tags={"[Central] Plan"},
     *   summary="List all plans",
     *   operationId="IndexPlan",
     *   security=false,
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     **/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        return response()->json($this->planRepository->getAll($request->all()));
    }

    /**
     * @OA\Post(
     ** path="/plans",
     *   tags={"[Central] Plan"},
     *   summary="Store new plan",
     *   operationId="StorePlan",
     *   security={ {"bearer": {}} },
     *
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new plan",
     *     @OA\JsonContent(
     *          required={"name","description","currency","benefits","unit_price","time_duration"},
     *          @OA\Property(property="name", type="string", format="string", example="Plan Premium"),
     *          @OA\Property(property="description", type="string", format="string", example="Expedita et est iste qui aut illum."),
     *          @OA\Property(property="currency", type="string", example="USD"),
     *          @OA\Property(property="benefits", type="object", example="[{'one':'plan 1'}]"),
     *          @OA\Property(property="unit_price", type="integer", example="253"),
     *          @OA\Property(property="time_duration", type="integer", example="365"),
     *    ),
     * ),
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                      "id": "N",
     *                      "name": "Plan Premium",
     *                      "description": "Expedita et est iste qui aut illum.",
     *                      "currency": "USD",
     *                      "benefits": "[{'one':'plan 1'}]",
     *                      "unit_price": "253",
     *                      "time_duration": "365"
     *                    }
     *                 )
     *             )
     *         }
     *     )
     *   ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return response()->json($this->planRepository->create($request->all()))->setStatusCode(201);
    }

    /**
     * @OA\Get(
     *   path="/plans/{id}",
     *   tags={"[Central] Plan"},
     *   summary="List all plans",
     *   operationId="ShowPlan",
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, $id)
    {
        return response()->json($this->planRepository->getById($id));
    }

     /**
     * @OA\Put(
     ** path="/plans/{id}",
     *   tags={"[Central] Plan"},
     *   summary="Update exists plan",
     *   operationId="UpdatePlan",
     *
     *   @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new plan",
     *     @OA\JsonContent(
     *          required={"name","currency","benefits","unit_price","time_duration"},
     *          @OA\Property(property="name", type="string", format="string", example="Plan Premium"),
     *          @OA\Property(property="description", type="string", format="string", example="Expedita et est iste qui aut illum."),
     *          @OA\Property(property="currency", type="string", example="USD"),
     *          @OA\Property(property="benefits", type="object", example="[{'one':'plan 1'}]"),
     *          @OA\Property(property="unit_price", type="integer", example="253"),
     *          @OA\Property(property="time_duration", type="integer", example="365"),
     *    ),
     * ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                       "id": 3,
     *                       "name": "Free",
     *                       "description": "Lorem",
     *                       "currency": "USD",
     *                       "benefits": {
     *                         "one": "+25 Usuarios",
     *                         "two": "+2 Correos"
     *                       },
     *                       "unit_price": 434.3,
     *                       "time_duration": 30
     *                    }
     *                 )
     *             )
     *         }
     *     ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     *)
     *
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->planRepository->UpdateById($id, $request->all()));
    }

    /**
     * @OA\Delete(
     *   path="/plans/{id}",
     *   tags={"[Central] Plan"},
     *   summary="List all plans",
     *   operationId="DeletePlan",
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, $id)
    {
        return response()->json($this->planRepository->destroy($id))->setStatusCode(204);
    }
}
