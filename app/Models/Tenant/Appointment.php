<?php

namespace App\Models\Tenant;


use App\Models\Tenant\User;
use App\Models\Tenant\Patient;
use App\Models\Tenant\Consultation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Appointment extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'urgency', 'scheduled_date', 'patient_id',
        'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'urgency' => 'boolean'
    ];

    /**
     * Get the consultation that owns the appointment
     *
     */
    public function consultations()
    {
        return $this->belongsTo(Consultation::class,
            'appointment_id'
        );
    }

    /**
     * Get the patient for the appointment
     *
     */
    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id');
    }

    /**
     * Get the user for the appointment
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');

    }

}
