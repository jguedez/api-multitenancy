<?php

namespace Database\Seeders\Central;

use App\Models\Central\Invoice;
use Illuminate\Database\Seeder;

class InvoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Invoice::factory()->count(3)->create();

    }
}
