<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Doctorfly API Multitenancy Documentation",
     *      @OA\Contact(
     *          email="admin@admin.com"
     *      ),
     * ),
     *
     *
     */
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
