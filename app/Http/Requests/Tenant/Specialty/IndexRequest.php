<?php

namespace App\Http\Requests\Tenant\Specialty;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\SpecialtyPolicy;

class IndexRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new SpecialtyPolicy(request()->header('x-tenant'), 'specialty.index');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
