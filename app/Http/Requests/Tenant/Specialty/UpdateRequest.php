<?php

namespace App\Http\Requests\Tenant\Specialty;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\SpecialtyPolicy;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new SpecialtyPolicy(request()->header('x-tenant'), 'specialty.update');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $specialty_id = $this->route()->parameters('specialties');
        $id = (int)$specialty_id['specialty'];

        return [
            'name'        => 'required|unique:specialties,name,'.$id,
            'description' => 'sometimes'
        ];
    }
}
