<?php

namespace App\Http\Requests\Tenant\Patient;

use App\Models\Tenant\Patient;
use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\PatientPolicy;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new PatientPolicy(request()->header('x-tenant'), 'patient.update');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $patient_id = $this->route()->parameters('patients');
        $patient = $this->findRecordInRequest(Patient::class, $patient_id['patient']);

        return [
            'first_name'        => 'required',
            'last_name'         => 'required',
            'age'               => 'required',
            'dni'               => 'required_without:person_id|unique:people,dni,'.$patient->person_id,
            'cuit'              => 'sometimes|unique:people,cuit',
            'nationality'       => 'sometimes',
            'picture'           => 'nullable',
            'gender'            => 'required',
            'birthday'          => 'sometimes',
            'marital_status'    => 'required',
            'phone'             => 'sometimes',
            'phone.landline'    => 'required_with:phone',
            'phone.movil'       => 'required_with:phone',
            'address'           => 'sometimes',
            'address.street'    => 'required_with:address',
            'address.country'   => 'required_with:address',
            'address.state'     => 'required_with:address',
            'address.city'      => 'required_with:address',
            'blood_type'        => 'sometimes',
            'race'              => 'sometimes',
            'occupation'        => 'required',
            'guardian'          => 'sometimes',
            'emergency_contact' => 'required',
            'status'            => 'required',
            'social_security'   => 'sometimes',
            'cod'               => 'sometimes|unique:histories,cod,'.$patient->histories->id,
            'allergies'         => 'sometimes',
            'medication'        => 'sometimes',
            'surgical_history'  => 'sometimes',
            'risk_factors'      => 'sometimes',
            'toxic_habits'      => 'sometimes',
            'person_id'         => 'sometimes|exists:people,id|unique:people,id,'.$patient->people->id,
            'history_id'        => 'sometimes|exists:histories,id|unique:histories,id,'.$patient->histories->id
        ];
    }
}
