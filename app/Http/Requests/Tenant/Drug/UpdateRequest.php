<?php

namespace App\Http\Requests\Tenant\Drug;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\DrugPolicy;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new DrugPolicy(request()->header('x-tenant'), 'drug.update');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $drug_id = $this->route()->parameters('drugs');

        return [
            'cod'         => 'required|unique:drugs,cod,'.(int)$drug_id['drug'],
            'description' => 'sometimes',
            'dosage'      => 'required',
            'duration'    => 'required',
            'dosage_form' => 'required',
            'comments'    => 'sometimes'
        ];
    }
}
