<?php

namespace App\Models\Tenant;

use App\Models\Tenant\Schedule;
use App\Models\Tenant\Specialty;
use App\Models\Tenant\Appointment;
use App\Models\Central\CentralUser;
use App\Models\Tenant\UserSchedule;
use Spatie\Permission\Traits\HasRoles;
use Stancl\Tenancy\Contracts\Syncable;
use Illuminate\Notifications\Notifiable;
use Stancl\Tenancy\Database\Concerns\ResourceSyncing;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements Syncable
{
    use HasFactory, Notifiable, ResourceSyncing, HasRoles;

    protected $guarded = [];
    public $timestamps = true;
    protected $guard_name = 'api';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'global_id', 'first_name', 'last_name', 'dni',
        'email', 'password', 'is_active', 'last_login',
        'address', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'global_id',
        'password',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active'        => 'boolean',
        'address'          => 'array',
        'phone'            => 'array'
    ];

    public function getGlobalIdentifierKey()
    {
        return $this->getAttribute($this->getGlobalIdentifierKeyName());
    }

    public function getGlobalIdentifierKeyName(): string
    {
        return 'global_id';
    }

    public function getCentralModelName(): string
    {
        return CentralUser::class;
    }

    public function getSyncedAttributeNames(): array
    {
        return [
            'first_name', 'last_name', 'dni',
            'email', 'password', 'is_active',
            'address', 'phone'
        ];
    }


    /**
     * Get basic user information.
     *
     * @return array
     */

    public function getBasicAttribute(){
        return [
            'id'       => $this->id,
            'fullname' => "{$this->first_name} {$this->last_name}",
        ];
    }

    /**
     * The schedules that belong to the user.
     *
     */
    public function schedules()
    {
        return $this->belongsToMany(Schedule::class)
        ->using(UserSchedule::class)
        ->withPivot([
            'day_id'
        ])->select('schedules.id', 'starts_at', 'ends_at', 'quotes', 'day_id', 'days.name as day_name')
        ->join('days', 'day_id', '=', 'days.id');
    }

    /**
     * The specialties that belong to the user.
     *
     */
    public function specialties()
    {
        return $this->belongsToMany(Specialty::class,
        'user_speciality',
        'user_id',
        'speciality_id'
        );
    }

    /**
     * Get the appointments for the user
     *
     */
    public function appointments()
    {
        return $this->hasMany(Appointment::class,
        'id',
        'user_id'
    );
    }

}
