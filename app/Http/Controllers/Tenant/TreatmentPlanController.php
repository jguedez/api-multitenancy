<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;
use App\Repositories\Tenant\TreatmentPlanRepository;
use App\Http\Requests\Tenant\TreatmentPlan\ShowRequest;
use App\Http\Requests\Tenant\TreatmentPlan\IndexRequest;
use App\Http\Requests\Tenant\TreatmentPlan\StoreRequest;
use App\Http\Requests\Tenant\TreatmentPlan\UpdateRequest;
use App\Http\Requests\Tenant\TreatmentPlan\DestroyRequest;

class TreatmentPlanController extends Controller
{

    protected $treatmentPlanRepository;

    /**
     * Treatment Plan Constructor.
     *
     * @param TreatmentPlanRepository $treatmentPlanRepository
     */

    public function __construct(TreatmentPlanRepository $treatmentPlanRepository)
    {
      $this->treatmentPlanRepository = $treatmentPlanRepository;
    }

    /**
     * @OA\Get(
     *   path="/treatment_plans",
     *   tags={"[Tenant] Treatment Plans"},
     *   summary="Lists all Treatment Plans associated with tenant",
     *   operationId="IndexTreatment Plans",
     *   security={ {"bearer": {}} },
     *
     * @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * )
     **/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        return response()->json($this->treatmentPlanRepository->getAll($request->all()));
    }

    /**
     * @OA\Post(
     ** path="/treatment_plans",
     *   tags={"[Tenant] Treatment Plans"},
     *   summary="Store new Treatment Plans",
     *   operationId="StoreTreatment Plans",
     *   security={ {"bearer": {}} },
     *
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new Treatment Plans",
     *     @OA\JsonContent(
     *          required={"plan", "status"},
     *          @OA\Property(property="plan", type="string", format="string", example="Omeprazol cada 8 horas por 2 dias"),
     *          @OA\Property(property="status", type="boolean", format="integer", example=true),
     *          @OA\Property(property="comments", type="string", format="string", example="Lorem ipsu"),
     *    )
     * ),
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
    */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return response()->json($this->treatmentPlanRepository->create($request->all()))->setStatusCode(201);
    }

    /**
     * @OA\Get(
     *   path="/treatment_plans/{id}",
     *   tags={"[Tenant] Treatment Plans"},
     *   summary="Details Treatment Plans",
     *   operationId="ShowTreatment Plans",
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, $id)
    {
        return response()->json($this->treatmentPlanRepository->getById($id));
    }

    /**
     * @OA\Put(
     ** path="/treatment_plans/{id}",
     *   tags={"[Tenant] Treatment Plans"},
     *   summary="Update new Treatment Plans",
     *   operationId="UpdateTreatment Plans",
     *   security={ {"bearer": {}} },
     *
     *    @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for update new Treatment Plans",
     *     @OA\JsonContent(
     *          required={"plan", "status"},
     *          @OA\Property(property="plan", type="string", format="string", example="Omeprazol cada 8 horas por 2 dias"),
     *          @OA\Property(property="status", type="boolean", format="integer", example=true),
     *          @OA\Property(property="comments", type="string", format="string", example="Lorem ipsu"),
     *    )
     * ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
    */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->treatmentPlanRepository->UpdateById($id, $request->all()));
    }

    /**
     * @OA\Delete(
     ** path="/treatment_plans/{id}",
     *   tags={"[Tenant] Treatment Plans"},
     *   summary="Update new Treatment Plans",
     *   operationId="UpdateTreatment Plans",
     *   security={ {"bearer": {}} },
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * )
     **/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, $id)
    {
        return response()->json($this->treatmentPlanRepository->destroy($id))->setStatusCode(204);
    }
}
