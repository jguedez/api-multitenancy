<?php

namespace App\Http\Requests\Tenant\Schedule;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\SchedulePolicy;

class IndexRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new SchedulePolicy(request()->header('x-tenant'), 'schedule.index');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
