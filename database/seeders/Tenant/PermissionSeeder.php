<?php

namespace Database\Seeders\Tenant;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::findOrCreate('patient.index');
        Permission::findOrCreate('patient.show');
        Permission::findOrCreate('patient.store');
        Permission::findOrCreate('patient.update');
        Permission::findOrCreate('patient.destroy');

        Permission::findOrCreate('appointment.index');
        Permission::findOrCreate('appointment.show');
        Permission::findOrCreate('appointment.store');
        Permission::findOrCreate('appointment.update');
        Permission::findOrCreate('appointment.destroy');

        Permission::findOrCreate('complaint.index');
        Permission::findOrCreate('complaint.show');
        Permission::findOrCreate('complaint.store');
        Permission::findOrCreate('complaint.update');
        Permission::findOrCreate('complaint.destroy');

        Permission::findOrCreate('consultation.index');
        Permission::findOrCreate('consultation.show');
        Permission::findOrCreate('consultation.store');
        Permission::findOrCreate('consultation.update');
        Permission::findOrCreate('consultation.destroy');

        Permission::findOrCreate('diagnostic.index');
        Permission::findOrCreate('diagnostic.show');
        Permission::findOrCreate('diagnostic.store');
        Permission::findOrCreate('diagnostic.update');
        Permission::findOrCreate('diagnostic.destroy');

        Permission::findOrCreate('drug.index');
        Permission::findOrCreate('drug.show');
        Permission::findOrCreate('drug.store');
        Permission::findOrCreate('drug.update');
        Permission::findOrCreate('drug.destroy');

        Permission::findOrCreate('history.index');
        Permission::findOrCreate('history.show');
        Permission::findOrCreate('history.store');
        Permission::findOrCreate('history.update');
        Permission::findOrCreate('history.destroy');

        Permission::findOrCreate('investigation.index');
        Permission::findOrCreate('investigation.show');
        Permission::findOrCreate('investigation.store');
        Permission::findOrCreate('investigation.update');
        Permission::findOrCreate('investigation.destroy');

        Permission::findOrCreate('schedule.index');
        Permission::findOrCreate('schedule.show');
        Permission::findOrCreate('schedule.store');
        Permission::findOrCreate('schedule.update');
        Permission::findOrCreate('schedule.destroy');

        Permission::findOrCreate('specialty.index');
        Permission::findOrCreate('specialty.show');
        Permission::findOrCreate('specialty.store');
        Permission::findOrCreate('specialty.update');
        Permission::findOrCreate('specialty.destroy');

        Permission::findOrCreate('treatmentPlan.index');
        Permission::findOrCreate('treatmentPlan.show');
        Permission::findOrCreate('treatmentPlan.store');
        Permission::findOrCreate('treatmentPlan.update');
        Permission::findOrCreate('treatmentPlan.destroy');

        Permission::findOrCreate('treatmentProc.index');
        Permission::findOrCreate('treatmentProc.show');
        Permission::findOrCreate('treatmentProc.store');
        Permission::findOrCreate('treatmentProc.update');
        Permission::findOrCreate('treatmentProc.destroy');

        Permission::findOrCreate('user.index');
        Permission::findOrCreate('user.show');
        Permission::findOrCreate('user.store');
        Permission::findOrCreate('user.update');
        Permission::findOrCreate('user.destroy');
    }
}
