<?php

namespace App\Http\Requests\Tenant\Complaint;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\ComplaintPolicy;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new ComplaintPolicy(request()->header('x-tenant'), 'complaint.store');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required',
            'description' => 'required'
        ];
    }
}
