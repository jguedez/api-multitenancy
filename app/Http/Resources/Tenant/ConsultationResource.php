<?php

namespace App\Http\Resources\Tenant;

use Illuminate\Http\Resources\Json\JsonResource;

class ConsultationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                   => $this->id,
            'cod'                  => $this->cod,
            'subject'              => $this->subject,
            'duration'             => $this->duration,
            'appointment'          => $this->appointments->patient->basic,
            'histories'            => $this->histories,
            'diagnostics'          => $this->diagnostics,
            'complaints'           => $this->complaits,
            'investigations'       => $this->investigation,
            'treatment_plans'      => $this->treatmentPlan,
            'treatment_procedures' => $this->treatmentProcedure,
            'drugs'                => $this->drugs,
        ];

    }
}
