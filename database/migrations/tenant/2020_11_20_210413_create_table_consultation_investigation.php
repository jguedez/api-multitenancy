<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableConsultationInvestigation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultation_investigation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('consultation_id');
            $table->unsignedBigInteger('investigation_id');
            $table->timestamps();

            $table->foreign('consultation_id')
            ->references('id')
            ->on('consultations')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('investigation_id')
            ->references('id')
            ->on('investigations')
            ->onUpdate('cascade')
            ->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultation_investigation');
    }
}
