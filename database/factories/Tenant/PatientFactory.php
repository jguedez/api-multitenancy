<?php

namespace Database\Factories\Tenant;

use App\Models\Tenant\Patient;
use Illuminate\Database\Eloquent\Factories\Factory;

class PatientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Patient::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'occupation'        => $this->faker->jobTitle,
            'guardian'          => [
                'name'        => $this->faker->name,
                'phone'       => $this->faker->phoneNumber,
                'relatinship' => $this->faker->randomElement(['friend', 'family', 'married']),
            ],
            'emergency_contact' => [
                'name'         => $this->faker->name,
                'relationship' => $this->faker->randomElement(['friend', 'family', 'married']),
                'phone'        => $this->faker->phoneNumber,
            ],
            'status'            => $this->faker->randomElement(['Entered', 'Pre-operation', 'Regular']),
            'social_security'   => $this->faker->ean8,
            'person_id'         => $this->faker->unique(true)->numberBetween(1, 50),
            'history_id'        => $this->faker->unique(true)->numberBetween(1, 50),
            'created_at'        => now(),
            'updated_at'        => now(),
        ];
    }
}
