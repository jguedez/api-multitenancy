<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableConsultationDrug extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultation_drug', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('consultation_id');
            $table->unsignedBigInteger('drug_id');
            $table->timestamps();

            $table->foreign('consultation_id')
            ->references('id')
            ->on('consultations')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('drug_id')
            ->references('id')
            ->on('drugs')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultation_drug');
    }
}
