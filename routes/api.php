<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Central\AuthController@login');
Route::post('register', 'Central\AuthController@register');
Route::post('forget-password', 'Central\AuthController@forget');
Route::post('reset-password', 'Central\AuthController@setUpdatePassword');
Route::get('plans', 'Central\PlanController@index');

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::post('logout', 'Central\AuthController@logout');
    Route::post('refresh', 'Central\AuthController@refresh');
    Route::Apiresource('plans', 'Central\PlanController')->except('index');
    Route::Apiresource('subscriptions', 'Central\SubscriptionController');
    Route::Apiresource('users', 'Central\UserController')->only(['show', 'update', 'destroy']);
    Route::Apiresource('invoices', 'Central\InvoiceController');
});



