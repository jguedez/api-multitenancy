<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;
use App\Repositories\Tenant\HistoryRepository;
use App\Http\Requests\Tenant\History\ShowRequest;
use App\Http\Requests\Tenant\History\IndexRequest;
use App\Http\Requests\Tenant\History\StoreRequest;
use App\Http\Requests\Tenant\History\UpdateRequest;
use App\Http\Requests\Tenant\History\DestroyRequest;

class HistoryController extends Controller
{
    protected $historyRepository;

    /**
     * History Constructor.
     *
     * @param HistoryRepository $historyRepository
     */

    public function __construct(HistoryRepository $historyRepository)
    {
      $this->historyRepository = $historyRepository;
    }

    /**
     * @OA\Get(
     *   path="/histories",
     *   tags={"[Tenant] Histories"},
     *   summary="Lists all Histories associated with tenant",
     *   operationId="IndexHistories",
     *   security={ {"bearer": {}} },
     *
     * @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * )
     **/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        return response()->json($this->historyRepository->getAll($request->all()));
    }

    /**
     * @OA\Post(
     ** path="/histories",
     *   tags={"[Tenant] Histories"},
     *   summary="Store new Histories",
     *   operationId="StoreHistories",
     *   security={ {"bearer": {}} },
     *
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new Histories",
     *     @OA\JsonContent(
     *          required={"cod"},
     *          @OA\Property(property="cod", type="string", format="string", example="HC-00001"),
     *          @OA\Property(property="allergies", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="medication", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="surgical_history", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="risk_factors", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="toxic_habits", type="string", format="string", example="Lorem ipsu"),
     *    )
     * ),
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
    */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return response()->json($this->historyRepository->create($request->all()))->setStatusCode(201);
    }

    /**
     * @OA\Get(
     *   path="/histories/{id}",
     *   tags={"[Tenant] Histories"},
     *   summary="Details Histories",
     *   operationId="ShowHistories",
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, $id)
    {
        return response()->json($this->historyRepository->getById($id));
    }

    /**
     * @OA\Put(
     ** path="/histories/{id}",
     *   tags={"[Tenant] Histories"},
     *   summary="Update new Histories",
     *   operationId="UpdateHistories",
     *   security={ {"bearer": {}} },
     *
     *    @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for update new Histories",
     *     @OA\JsonContent(
    *          required={"cod"},
     *          @OA\Property(property="cod", type="string", format="string", example="HC-00001"),
     *          @OA\Property(property="allergies", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="medication", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="surgical_history", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="risk_factors", type="string", format="string", example="Lorem ipsu"),
     *          @OA\Property(property="toxic_habits", type="string", format="string", example="Lorem ipsu"),
     *    )
     * ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
    */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->historyRepository->UpdateById($id, $request->all()));
    }

    /**
     * @OA\Delete(
     ** path="/histories/{id}",
     *   tags={"[Tenant] Histories"},
     *   summary="Update new Histories",
     *   operationId="UpdateHistories",
     *   security={ {"bearer": {}} },
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * )
     **/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, $id)
    {
        return response()->json($this->historyRepository->destroy($id))->setStatusCode(204);
    }
}
