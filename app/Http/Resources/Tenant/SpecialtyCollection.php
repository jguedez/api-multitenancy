<?php

namespace App\Http\Resources\Tenant;

use App\Http\Resources\ApiCollection;
use App\Http\Resources\Tenant\SpecialtyResource;

class SpecialtyCollection extends ApiCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data'  => SpecialtyResource::collection($this->collection),
            'meta'  => $this->dataMeta($request),
            'links' => $this->dataLinks($request)
        ];
    }
}
