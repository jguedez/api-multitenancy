<?php

namespace App\Http\Controllers\Central;

use Illuminate\Support\Str;
use App\Mail\RecoveryPassword;
use Illuminate\Support\Facades\DB;
use App\Models\Central\CentralUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\Central\Auth\LoginRequest;
use App\Http\Requests\Central\Auth\UpdatePassword;
use App\Http\Requests\Central\Auth\RegisterRequest;
use App\Http\Requests\Central\Auth\ResetPasswordRequest;

class AuthController extends Controller
{
    /**
     * @OA\Post(
     ** path="/login",
     *   tags={"[Universal] - Authentication"},
     *   summary="Login",
     *   operationId="login",
     *
     *   @OA\RequestBody(
     *      required=true,
     *      description="Login the user",
     *     @OA\JsonContent(
     *          required={"email","password"},
     *          @OA\Property(property="email", type="string", format="email", example="newuser@gmail.com"),
     *          @OA\Property(property="password", type="string", format="string", example="secret")
     *    ),
     * ),
     * *   @OA\Response(
     *      response=200,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                      "not"
     *                    }
     *                 )
     *             )
     *         }
     *     ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     */

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request){

        if (!$token = auth()->attempt($request->validated())) {
            return response()->json(['error' => 'The credentials are invalid'], 401);
        }

        if ($this->verifyStatus($request->email))
        {
            return $this->createNewToken($token);
        }

        return response()->json(['message' => 'The user is disabled, please contact support'])->setStatusCode(401);
    }

    /**
     * @OA\Post(
     ** path="/register",
     *   tags={"[Universal] - Authentication"},
     *   summary="Register",
     *   operationId="register",
     *
     *   @OA\RequestBody(
     *      required=true,
     *      description="Create a new user",
     *     @OA\JsonContent(
     *          required={"email","password","first_name","last_name","dni"},
     *          @OA\Property(property="email", type="string", format="email", example="newuser@gmail.com"),
     *          @OA\Property(property="password", type="string", format="string", example="secret"),
     *          @OA\Property(property="first_name", type="string", example="Pedro"),
     *          @OA\Property(property="last_name", type="string", example="Perez"),
     *          @OA\Property(property="dni", type="integer", example="25343433")
     *    ),
     * ),
     * *   @OA\Response(
     *      response=201,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                      "email": "dfsdfds@gmail.com",
     *                      "first_name": "Erika",
     *                      "last_name": "Colmenares",
     *                      "dni": "235423434",
     *                      "is_active": true,
     *                      "global_id": "6ea8ks2",
     *                      "id": 9
     *                    }
     *                 )
     *             )
     *         }
     *     ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   )
     *)
     *
     */
    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request) {

        $user = CentralUser::create(array_merge(
                    $request->validated(),
                    ['password' => bcrypt($request->password),
                    'is_active' => true,
                    'global_id' => $this->generateGlobalId($request->all())]
                ));

        return response()->json($user, 201);
    }

    /**
     * @OA\Post(
     ** path="/logout",
     *   tags={"[Universal] - Authentication"},
     *   summary="logout",
     *   operationId="logout",
     *
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   security={ {"bearer": {}} }
     *)
     *
     */
    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * @OA\Post(
     ** path="/refresh",
     *   tags={"[Universal] - Authentication"},
     *   summary="refresh",
     *   operationId="refresh",
     *
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   security={ {"bearer": {}} }
     *)
     *
     */
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

  /**
     * @OA\Post(
     ** path="/forget-password",
     *   tags={"[Universal] - Authentication"},
     *   summary="forget password",
     *   operationId="forget",
     *
     *   @OA\RequestBody(
     *      required=true,
     *      description="Forget password",
     *     @OA\JsonContent(
     *          required={"email","password","first_name","last_name","dni"},
     *          @OA\Property(property="email", type="string", format="email", example="newuser@gmail.com"),
     *    ),
     * ),
     * *   @OA\Response(
     *      response=200,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                      "email": "dfsdfds@gmail.com",
     *                      "first_name": "Erika",
     *                      "last_name": "Colmenares",
     *                      "dni": "235423434",
     *                      "is_active": true,
     *                      "global_id": "6ea8ks2",
     *                      "id": 9
     *                    }
     *                 )
     *             )
     *         }
     *     ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   )
     *)
     *
     */
    /**
     * Recover password.
     *
     * @param  string $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function forget(UpdatePassword $request)
    {
        $user = CentralUser::where('email', $request['email'])->first();
        $code = $this->genereteCode($user);
        Mail::to($user->email)->send(new RecoveryPassword($user, $code->token));

        return response()->json(['message' => 'An email has been sent for password reset']);
    }

    /**
     * @OA\Post(
     ** path="/reset-password",
     *   tags={"[Universal] - Authentication"},
     *   summary="Reset Password",
     *   operationId="Reset Password",
     *
     *   @OA\Parameter(
     *      name="token",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="Forget password",
     *     @OA\JsonContent(
     *          required={"password","password_confirmed"},
     *          @OA\Property(property="password", type="string", format="string", example="secret"),
     *          @OA\Property(property="password_confirmed", type="string", format="string", example="secret"),
     *    ),
     * ),
     * *   @OA\Response(
     *      response=200,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={"Your password has been successfully reset, please login"}
     *                 )
     *             )
     *         }
     *     ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   )
     *)
     *
     */
    /**
     * Update Password
     *
     * @param  string $token
     * @param  request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function setUpdatePassword(ResetPasswordRequest $request)
    {
        if($this->verifyToken($request->token))
        {
            return $this->postResetPassword($request->all(), $this->verifyToken($request->token));
        }

        return response()->json(['message' => 'Your request has expired, please return a new password reset']);
    }

    /**
     * Validate the token
     *
     * @return User or Null
     */
    protected function verifyToken($token)
    {
       $resetToken = DB::table('password_resets')->where('token', $token)->first();

       if (empty($resetToken))
       {
           return null;
       }

       $user = CentralUser::where('email', $resetToken->email)->first();

       if (!empty($resetToken->created_at) && $resetToken->created_at <= now()->addMinutes(30))
       {
           return $user->id;
       }else {
           return null;
       }
    }

    private function postResetPassword($request, $user)
    {
        $user = CentralUser::findOrFail($user);
        $user->password = bcrypt($request['password']);
        $user->save();
        DB::table('password_resets')->where('email', $user->email)->delete();

        return response()->json(['message' => 'Your password has been successfully reset, please login']);
    }

    private function genereteCode($user)
    {
        DB::table('password_resets')->where('email', $user->email)->delete();
        DB::table('password_resets')->Insert([
            'email' => $user->email,
            'token' => Str::random(64),
            'created_at' => now()
        ]);

        return DB::table('password_resets')
            ->where('email', $user->email)
            ->first();
    }

    private function verifyStatus($email)
    {
        $user = CentralUser::where('email', $email)->first();

        if ($user->is_active == true)
        {
            $user->last_login = now();
            return $user->save();
        }

        return false;
    }

    protected function generateGlobalId($data)
    {
        return str_shuffle(substr($data['first_name'], 3).rand(2, 2000).substr($data['last_name'], -2));
    }
}
