<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Repositories\Central\UserRepository;
use App\Http\Requests\Central\User\ShowRequest;
use App\Http\Requests\Central\User\UpdateRequest;
use App\Http\Requests\Central\User\DestroyRequest;

class UserController extends Controller
{
    protected $userRepository;

    /**
     * User Constructor.
     *
     * @param UserRepository $userRepository
     */

    public function __construct(UserRepository $userRepository)
    {
      $this->userRepository = $userRepository;
    }

    /**
     * @OA\Get(
     *   path="/users/{id}",
     *   tags={"[Central] Users"},
     *   summary="Details Userses",
     *   operationId="ShowUsers",
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, $id)
    {
        return response()->json($this->userRepository->getById($id));
    }

    /**
     * @OA\Put(
     ** path="/users/{id}",
     *   tags={"[Central] Users"},
     *   summary="Update Users",
     *   operationId="UpdateUsers",
     *   security={ {"bearer": {}} },
     *
     *  @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new Users",
     *     @OA\JsonContent(
     *          required={"dni", "email"},
     *          @OA\Property(property="email", type="string", format="email", example="newuser@gmail.com"),
     *          @OA\Property(property="password", type="string", format="string", example="secret"),
     *          @OA\Property(property="first_name", type="string", example="Pedro"),
     *          @OA\Property(property="last_name", type="string", example="Perez"),
     *          @OA\Property(property="dni", type="integer", example="25343433"),
     *          @OA\Property(property="address", type="object", example={"street":"848 Considine Keys\nGertrudeside, PA 63260","country":"Korea","state":"California","city":"Stoltenbergberg"}),
     *          @OA\Property(property="phone", type="object", example={"landline":"+5964065417527","movil":"+7496505982925"}),
     *    ),
     * ),
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->userRepository->UpdateById($id, $request->all()));
    }

    /**
     * @OA\Delete(
     *   path="/users/{id}",
     *   tags={"[Central] Users"},
     *   summary="Delete Users",
     *   operationId="DeleteUsers",
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, $id)
    {
        return response()->json($this->userRepository->destroy($id))->setStatusCode(204);
    }
}
