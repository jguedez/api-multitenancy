<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\Tenant\DaySeeder;
use Database\Seeders\Tenant\DrugSeeder;
use Database\Seeders\Tenant\RoleSeeder;
use Database\Seeders\Tenant\UserSeeder;
use Database\Seeders\Tenant\PersonSeeder;
use Database\Seeders\Tenant\HistorySeeder;
use Database\Seeders\Tenant\PatientSeeder;
use Database\Seeders\Tenant\ScheduleSeeder;
use Database\Seeders\Tenant\ComplaintSeeder;
use Database\Seeders\Tenant\SpecialtySeeder;
use Database\Seeders\Tenant\DiagnosticSeeder;
use Database\Seeders\Tenant\PermissionSeeder;
use Database\Seeders\Tenant\AppointmentSeeder;
use Database\Seeders\Tenant\ConsultationSeeder;
use Database\Seeders\Tenant\InvestigationSeeder;
use Database\Seeders\Tenant\TreatmentPlanSeeder;
use Database\Seeders\Tenant\RolePermissionSeeder;
use Database\Seeders\Tenant\TreatmentProcedureSeeder;


class TenantDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            DrugSeeder::class,
            DiagnosticSeeder::class,
            ComplaintSeeder::class,
            InvestigationSeeder::class,
            TreatmentPlanSeeder::class,
            TreatmentProcedureSeeder::class,
            PersonSeeder::class,
            HistorySeeder::class,
            PatientSeeder::class,
            DaySeeder::class,
            SpecialtySeeder::class,
            UserSeeder::class,
            ScheduleSeeder::class,
            AppointmentSeeder::class,
            ConsultationSeeder::class,
            RoleSeeder::class,
            PermissionSeeder::class,
            RolePermissionSeeder::class
        ]);
    }
}
