<?php

namespace App\Http\Resources\Tenant;

use Illuminate\Http\Resources\Json\JsonResource;

class TreatmentProcedureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'       => $this->id,
            'type'     => $this->type,
            'name'     => $this->name,
            'comments' => $this->comments,
            'created_at'  => ($this->created_at)->format('Y-m-d H:m:s'),
            'updated_at'  => ($this->updated_at)->format('Y-m-d H:m:s'),
        ];
    }
}
