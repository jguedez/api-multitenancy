<?php

namespace App\Http\Requests\Tenant\User;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\UserPolicy;

class ShowRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new UserPolicy(request()->header('x-tenant'), 'user.show');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
