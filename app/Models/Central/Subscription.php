<?php

namespace App\Models\Central;


use App\Models\Central\Plan;
use App\Models\Central\Invoice;
use App\Models\Central\CentralUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Subscription extends Model
{
    use HasFactory;

    public $table = 'subcriptions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_at', 'ends_at', 'user_id', 'plan_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_at'   => 'datetime:Y-m-d H:m:s',
        'ends_at'    => 'datetime:Y-m-d H:m:s',
        'created_at' => 'datetime:Y-m-d H:m:s',
        'updated_at' => 'datetime:Y-m-d H:m:s'
    ];

       /**
     * Get basic user information.
     *
     * @return array
     */

    public function getGenericAttribute(){
        return [
            'id'         => $this->id,
            'start_at'   => ($this->start_at)->format('Y-m-d H:m:s'),
            'ends_at'    => ($this->ends_at)->format('Y-m-d H:m:s'),
            'user'       => [
                'id'     => $this->user->id,
                'full_name' => $this->user->first_name.' '.$this->user->last_name,
                'email'     => $this->user->email,
                'phone'     => $this->user->phone['landline'].' '.$this->user->phone['movil']
            ],
            'plan'       => $this->plan
        ];
    }

    /**
     * Get the plan that owns the subscription.
     */
    public function plan()
    {
        return $this->belongsTo(Plan::class, 'plan_id', 'id');
    }

    /**
     * Get the subscriptions the user.
     */
    public function user()
    {
        return $this->belongsTo(CentralUser::class, 'user_id');
    }

    /**
     * Get the invoice that owns the subscription.
     */
    public function invoice()
    {
        return $this->hasMany(Invoice::class, 'subscription_id', 'id');
    }

}

