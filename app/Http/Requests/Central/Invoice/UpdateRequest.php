<?php

namespace App\Http\Requests\Central\Invoice;

use App\Models\Central\Invoice;
use App\Http\Requests\ApiRequest;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $invoice_id = $this->route()->parameters('invoices');
        $invoice = $this->findRecordInRequest(Invoice::class, (int)$invoice_id['invoice']);

        return [
            'subject'                   => 'sometimes',
            'id_payment'                => 'required|unique:invoices,id_payment,'.$invoice->id,
            'due_date'                  => 'sometimes|date',
            'excise_duty'               => 'sometimes',
            'tax'                       => 'sometimes|numeric',
            'currency'                  => 'required|string',
            'shipping_address'          => 'sometimes',
            'shipping_address.street'   => 'required_with:shipping_address',
            'shipping_address.country'  => 'required_with:shipping_address',
            'shipping_address.state'    => 'required_with:shipping_address',
            'shipping_address.city'     => 'required_with:shipping_address',
            'details'                   => 'sometimes',
            'details.article'           => 'required_with:details',
            'details.description'       => 'required_with:details',
            'details.amount'            => 'required_with:details',
            'total'                     => 'required|numeric',
            'subscription_id'           => 'required|integer|exists:subcriptions,id|unique:invoices,subscription_id,'.$invoice->subscription_id,
        ];
    }
}
