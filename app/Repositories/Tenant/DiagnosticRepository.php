<?php

namespace App\Repositories\Tenant;

use App\Models\Tenant\Diagnostic;
use App\Repositories\BaseInterface;
use App\Http\Resources\Tenant\DiagnosticResource;
use App\Http\Resources\Tenant\DiagnosticCollection;

class DiagnosticRepository implements BaseInterface
{
    protected $model;
    protected $diagnostic;

    /**
     * Diagnostic Repository constructor.
     * @param Drug $diagnostic
     */
    public function __construct(Diagnostic $diagnostic)
    {
        $this->model = $diagnostic;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new DiagnosticCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new = $this->model::create($data);
        return new DiagnosticResource($new);
    }

    public function getById($id)
    {
        return new DiagnosticResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $this->model::findOrFail($id)->update($data);
        return $this->getById($id);
    }
}
