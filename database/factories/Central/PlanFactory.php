<?php

namespace Database\Factories\Central;

use App\Models\Central\Plan;
use Illuminate\Database\Eloquent\Factories\Factory;

class PlanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Plan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'          => $this->faker->word,
            'description'   => $this->faker->text(60),
            'currency'      => $this->faker->currencyCode,
            'unit_price'    => $this->faker->randomNumber(2),
            'benefits'      => [
                'one' => '+'.$this->faker->randomNumber(2).' Usuarios',
                'two' => '+'.$this->faker->randomNumber(2).' Emails'
            ],
            'time_duration' => $this->faker->randomNumber(2),
        ];
    }
}
