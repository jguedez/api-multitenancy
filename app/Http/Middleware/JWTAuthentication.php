<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JWTAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($user = JWTAuth::parseToken()->authenticate())
        {
            if ($user->global_id != $request->header('x-tenant'))
            {
                return response()->json(['errors' => 'You do not have access to this tenant'], 401);
            }

            return $next($request);
        }
    }
}

