<?php

namespace Database\Seeders\Tenant;

use App\Models\Tenant\User;
use App\Models\Central\Tenant;
use Illuminate\Database\Seeder;
use App\Models\Central\CentralUser;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $central_one = CentralUser::findOrFail(1);
       $tenant_one = Tenant::find($central_one->global_id);
       tenancy()->initialize($tenant_one);

       $user_one = User::findOrFail(1);

       $data = [
        'password'   => bcrypt('secret'),
        'first_name' => $central_one->first_name,
        'last_name'  => $central_one->last_name,
        'dni'        => $central_one->dni,
        'email'      => $central_one->email,
        'is_active'  => true
       ];
       $user_one->fill($data);
       $user_one->save();
    }
}
