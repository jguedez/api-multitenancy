<?php

namespace App\Http\Requests\Central\Plan;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|unique:plans,name',
            'description'   => 'required',
            'currency'      => 'required',
            'benefits'      => 'required',
            'unit_price'    => 'required|numeric',
            'time_duration' => 'required|numeric',
        ];
    }
}
