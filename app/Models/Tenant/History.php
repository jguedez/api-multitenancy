<?php

namespace App\Models\Tenant;

use App\Models\Tenant\Patient;
use App\Models\Tenant\Consultation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class History extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cod', 'allergies', 'medication', 'surgical_history',
        'risk_factors', 'toxic_habits'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'pivot'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    /**
     * The consultations that belong to the history.
     *
     */

    public function consultations()
    {
        return $this->belongsToMany(Consultation::class,
            'history_consultation',
            'consultation_id',
            'history_id'
        );
    }

    /**
     * Get the history that owns the person.
     *
     */
    public function patient()
    {
        return $this->belongsTo(Patient::class, 'history_id');
    }
}
