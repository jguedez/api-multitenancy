<?php

namespace App\Repositories\Tenant;

use App\Models\Tenant\Patient;
use App\Repositories\BaseInterface;
use App\Repositories\Tenant\PersonRepository;
use App\Http\Resources\Tenant\PatientResource;
use App\Repositories\Tenant\HistoryRepository;
use App\Http\Resources\Tenant\PatientCollection;

class PatientRepository implements BaseInterface
{
    protected $model;
    protected $patient, $people, $history;

    /**
     * Patient Repository constructor.
     * @param Patient $patient
     */
    public function __construct(Patient $patient, PersonRepository $people, HistoryRepository $history)
    {
        $this->model = $patient;
        $this->people = $people;
        $this->history = $history;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new PatientCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new_patient = new Patient();
        $new_patient->fill($data);

        if (empty($data['person_id']))
        {
            $new_person = $this->people->create($data);
            $new_patient->person_id = $new_person->id;
        }

        if (empty($data['history_id']))
        {
            $new_history = $this->history->create($data, $new_person);
            $new_patient->history_id = $new_history->id;
        }

        $new_patient->save();

        return new PatientResource($new_patient);
    }

    public function getById($id)
    {
        return new PatientResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        $patient = $this->getById($id);

        $this->people->destroy($patient['person_id']);
        $this->history->destroy($patient['history_id']);

        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $patient = $this->getById($id);
        $patient->update($data);

        $this->people->UpdateById($patient['person_id'], $data);
        $this->history->UpdateById($patient['history_id'], $data);

        return $patient;
    }

    protected function setPatient($data, $key)
    {
        $key['history_id'] = null;

        $new_patient = $this->model::create($key);

        return new PatientResource($new_patient);
    }

    protected function newPatient($data)
    {
        $new_patient = new Patient();
        $new_patient->fill($data);

        if (!isset($data['person_id']))
        {
            $new_person = $this->people->create($data);
            $new_patient->person_id = $new_person->id;
        }

        if (!isset($data['history_id']))
        {
            $new_history = $this->history->create($data);
            $new_patient->person_id = $new_history->id;
        }

        $new_patient->save();

        return new PatientResource($new_patient);
    }
}
