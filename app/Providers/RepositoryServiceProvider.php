<?php

namespace App\Providers;

use App\Repositories\BaseInterface;
use App\Repositories\Central\InvoiceRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Central\PlanRepository;
use App\Repositories\Central\UserRepository;
use App\Repositories\Central\SubscriptionRepository;
use App\Repositories\Tenant\ComplaintRepository;
use App\Repositories\Tenant\DiagnosticRepository;
use App\Repositories\Tenant\DrugRepository;
use App\Repositories\Tenant\HistoryRepository;
use App\Repositories\Tenant\InvestigationRepository;
use App\Repositories\Tenant\PatientRepository;
use App\Repositories\Tenant\PersonRepository;
use App\Repositories\Tenant\TreatmentPlanRepository;
use App\Repositories\Tenant\TreatmentProcedureRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            BaseInterface::class,
            UserRepository::class,
            SubscriptionRepository::class,
            PlanRepository::class,
            InvoiceRepository::class,
            PatientRepository::class,
            HistoryRepository::class,
            PersonRepository::class,
            DrugRepository::class,
            InvestigationRepository::class,
            DiagnosticRepository::class,
            TreatmentPlanRepository::class,
            TreatmentProcedureRepository::class,
            ComplaintRepository::class,
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
