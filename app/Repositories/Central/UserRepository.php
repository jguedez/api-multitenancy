<?php

namespace App\Repositories\Central;

use App\Models\Central\CentralUser;
use App\Repositories\BaseInterface;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Central\UserResource;

class UserRepository implements BaseInterface
{
    protected $model;
    protected $user;

    /**
     * User Repository constructor.
     * @param CentralUser $user
     */
    public function __construct(CentralUser $user)
    {
        $this->model = $user;
    }

    public function getAll($data)
    {

    }
    public function create(array $data)
    {

    }

    public function getById($id)
    {
        if ($this->verifyUser($id))
        {
            return ['errors' => 'You do not have permission to view this user'];
        }

        return new UserResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {

        if ($this->verifyUser($id))
        {
            return ['errors' => 'You do not have permission to update this user'];
        }

        $user = $this->getById($id);
        $user->fill($data);

        if (isset($data['password']))
        {
            $user['password'] = bcrypt($data['password']);
        }

        $user->save();

        return new UserResource($this->model::find($id));
    }

    protected function verifyUser($id)
    {
        if ($id != Auth::user()->id)
        {
            return ['errors' => 'You do not have permission to view this user'];
        }
    }
}
