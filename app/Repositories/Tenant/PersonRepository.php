<?php

namespace App\Repositories\Tenant;

use App\Models\Tenant\Person;
use App\Repositories\BaseInterface;

class PersonRepository implements BaseInterface
{
    protected $model;
    protected $person;

    /**
     * Person Repository constructor.
     * @param Person $person
     */
    public function __construct(Person $person)
    {
        $this->model = $person;
    }

    public function getAll($data)
    {
        return false;
    }

    public function create(array $data)
    {
        return $this->model::create($data);
    }

    public function getById($id)
    {
        return false;
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        return $this->model::findOrFail($id)->update($data);
    }

}
