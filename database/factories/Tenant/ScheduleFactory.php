<?php

namespace Database\Factories\Tenant;

use App\Models\Tenant\Schedule;
use Illuminate\Database\Eloquent\Factories\Factory;

class ScheduleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Schedule::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'starts_at' => $this->faker->unique()->randomElement(['7:00 AM', "1:00 PM", "6:00 PM"]),
            'ends_at'   => $this->faker->unique()->randomElement(['12:00 PM', "5:00 PM", "9:00 PM"]),
            'quotes'    => $this->faker->unique()->randomElement([5, 6, 10]),
        ];
    }
}
