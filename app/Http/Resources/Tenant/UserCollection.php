<?php

namespace App\Http\Resources\Tenant;

use App\Http\Resources\ApiCollection;
use App\Http\Resources\Tenant\UserResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ApiCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data'  => UserResource::collection($this->collection),
            'meta'  => $this->dataMeta($request),
            'links' => $this->dataLinks($request)
        ];
    }
}
