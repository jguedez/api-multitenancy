<?php

namespace App\Models\Tenant;

use App\Models\Tenant\Person;
use App\Models\Tenant\History;
use App\Models\Tenant\Appointment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Patient extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'occupation', 'guardian', 'emergency_contact',
        'status', 'social_security', 'person_id', 'history_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'guardian'          => 'array',
        'emergency_contact' => 'array'
    ];

      /**
     * Get basic patient information.
     *
     * @return array
     */

    public function getBasicAttribute(){
        return [
            'patient_id'        => $this->id,
            'history_id'        => $this->histories->id,
            'person_id'         => $this->people->id,
            'fullname'          => $this->people->fullname,
            'age'               => $this->people->age,
            'dni'               => $this->people->dni,
            'gender'            => $this->people->gender,
            'guardian'          => $this->guardian,
            'emergency_contact' => $this->emergency_contact,
        ];
    }

    /**
     * Get the person record associated with the pacient.
     *
     */
    public function people()
    {
        return $this->hasOne(Person::class,
            'id',
            'person_id'
        );
    }

    /**
     * Get the history associated with the pacient
     *
     */
    public function histories()
    {
        return $this->hasOne(History::class,
            'id',
            'history_id'
        );
    }

    /**
     * Get the appointments for the patient
     *
     */
    public function appointment()
    {
        return $this->hasMany(Appointment::class,
        'patient_id',
        'id'
    );
    }

}
