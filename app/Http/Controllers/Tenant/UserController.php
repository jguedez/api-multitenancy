<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;
use App\Repositories\Tenant\UserRepository;
use App\Http\Requests\Central\User\ShowRequest;
use App\Http\Requests\Tenant\User\IndexRequest;
use App\Http\Requests\Tenant\User\StoreRequest;
use App\Http\Requests\Tenant\User\UpdateRequest;
use App\Http\Requests\Tenant\User\DestroyRequest;

class UserController extends Controller
{
    protected $userRepository;

    /**
     * User Constructor.
     *
     * @param UserRepository $userRepository
     */

    public function __construct(UserRepository $userRepository)
    {
      $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        return response()->json($this->userRepository->getAll($request->all()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return response()->json($this->userRepository->create($request->all()))->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, $id)
    {
        return response()->json($this->userRepository->getById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->userRepository->UpdateById($id, $request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, $id)
    {
        return response()->json($this->userRepository->destroy($id))->setStatusCode(204);
    }
}
