<?php

namespace Database\Seeders\Tenant;

use App\Models\Tenant\Drug;
use Illuminate\Database\Seeder;

class DrugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Drug::factory()->count(10)->create();
    }
}
