<?php

namespace App\Http\Resources\Tenant;

use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'             => $this->id,
            'urgency'        => $this->urgency,
            'scheduled_date' => $this->scheduled_date,
            'patient'        => $this->patient->basic,
            'user'           => $this->user->basic,
            'created_at'  => ($this->created_at)->format('Y-m-d H:m:s'),
            'updated_at'  => ($this->updated_at)->format('Y-m-d H:m:s'),
        ];
    }

}

