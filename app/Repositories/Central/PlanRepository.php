<?php

namespace App\Repositories\Central;

use App\Models\Central\Plan;
use App\Repositories\BaseInterface;
use App\Http\Resources\Central\PlanResource;
use App\Http\Resources\Central\PlanCollection;

class PlanRepository implements BaseInterface
{
    protected $model;
    protected $plan;

    /**
     * Plan Repository constructor.
     * @param Plan $plan
     */
    public function __construct(Plan $plan)
    {
        $this->model = $plan;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new PlanCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new = $this->model::create($data);
        return new PlanResource($new);
    }

    public function getById($id)
    {
        return new PlanResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $this->model::findOrFail($id)->update($data);
        return $this->getById($id);
    }
}
