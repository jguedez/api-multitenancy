<?php

namespace App\Http\Requests\Tenant\Patient;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\PatientPolicy;

class IndexRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new PatientPolicy(request()->header('x-tenant'), 'patient.index');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
