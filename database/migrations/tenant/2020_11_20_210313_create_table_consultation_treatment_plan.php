<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableConsultationTreatmentPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultation_treatment_plan', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('consultation_id');
            $table->unsignedBigInteger('treatment_plan_id');
            $table->timestamps();

            $table->foreign('consultation_id')
            ->references('id')
            ->on('consultations')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('treatment_plan_id')
            ->references('id')
            ->on('treatment_plans')
            ->onUpdate('cascade')
            ->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultation_treatment_plan');
    }
}
