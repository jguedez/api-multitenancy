<?php

namespace App\Policies\Tenant;

use App\Policies\ApiPolicies;
use Illuminate\Auth\Access\HandlesAuthorization;

class AppointmentPolicy extends ApiPolicies
{
    use HandlesAuthorization;

    public $x_tenant;
    public $permission;

    public function __construct($x_tenant, $permission)
    {
        $this->x_tenant = $x_tenant;
        $this->permission = $permission;
    }

    /**
     * Determine whether the user can resources models.
     *
     * @return mixed
     */
    public function check()
    {
        return $this->checkPermission($this->x_tenant, $this->permission);
    }
}
