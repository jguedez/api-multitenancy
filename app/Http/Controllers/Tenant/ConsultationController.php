<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;
use App\Repositories\Tenant\ConsultationRepository;
use App\Http\Requests\Tenant\Consultation\ShowRequest;
use App\Http\Requests\Tenant\Consultation\IndexRequest;
use App\Http\Requests\Tenant\Consultation\StoreRequest;
use App\Http\Requests\Tenant\Consultation\UpdateRequest;
use App\Http\Requests\Tenant\Consultation\DestroyRequest;

class ConsultationController extends Controller
{
    protected $consultationRepository;

    /**
     * Consultation Constructor.
     *
     * @param ConsultationRepository $consultationRepository
     */

    public function __construct(ConsultationRepository $consultationRepository)
    {
      $this->consultationRepository = $consultationRepository;
    }

    /**
     * @OA\Get(
     *   path="/consultations",
     *   tags={"[Tenant] Consultations"},
     *   summary="Lists all Consultations associated with tenant",
     *   operationId="IndexConsultations",
     *   security={ {"bearer": {}} },
     *
     * @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * )
     **/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        return response()->json($this->consultationRepository->getAll($request->all()));

    }

    /**
     * @OA\Post(
     ** path="/consultations",
     *   tags={"[Tenant] Consultations"},
     *   summary="Store new Consultations",
     *   operationId="StoreConsultations",
     *   security={ {"bearer": {}} },
     *
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new Consultations",
     *     @OA\JsonContent(
     *          required={"cod","subject","duration","appointment_id","histories","diagnostics","complaints","investigations","treatment_plans"},
     *          @OA\Property(property="code", type="string", format="string", example="CON-X034893"),
     *          @OA\Property(property="subject", type="string", format="string", example="Rutina"),
     *          @OA\Property(property="duration", type="string", format="time", example="30:00"),
     *          @OA\Property(property="appointment_id", type="integer", format="integer", example="6"),
     *          @OA\Property(property="histories", type="string", format="array", example="[1, 2, 4]"),
     *          @OA\Property(property="diagnostics", type="string", format="array", example="[1, 2, 4]"),
     *          @OA\Property(property="treatment_plans", type="string", format="array", example="[1, 2, 4]"),
     *          @OA\Property(property="investigations", type="string", format="array", example="[1, 2, 4]"),
     *          @OA\Property(property="treatment_procedures", type="string", format="array", example="[1, 2, 4]"),
     *          @OA\Property(property="drugs", type="string", format="array", example="[1, 2, 4]"),
     *    )
     * ),
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
    */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return response()->json($this->consultationRepository->create($request->all()))->setStatusCode(201);
    }

    /**
     * @OA\Get(
     *   path="/consultations/{id}",
     *   tags={"[Tenant] Consultations"},
     *   summary="Details Consultations",
     *   operationId="ShowConsultations",
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, $id)
    {
        return response()->json($this->consultationRepository->getById($id));
    }

     /**
     * @OA\Put(
     ** path="/consultations/{id}",
     *   tags={"[Tenant] Consultations"},
     *   summary="Update new Consultations",
     *   operationId="UpdateConsultations",
     *   security={ {"bearer": {}} },
     *
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for update new Consultations",
     *     @OA\JsonContent(
     *          required={"cod"},
     *          @OA\Property(property="code", type="string", format="string", example="CON-X034893"),
     *          @OA\Property(property="subject", type="string", format="string", example="Rutina"),
     *          @OA\Property(property="duration", type="string", format="time", example="30:00"),
     *          @OA\Property(property="appointment_id", type="integer", format="integer", example="6"),
     *          @OA\Property(property="histories", type="string", format="array", example="[1, 2, 4]"),
     *          @OA\Property(property="diagnostics", type="string", format="array", example="[1, 2, 4]"),
     *          @OA\Property(property="treatment_plans", type="string", format="array", example="[1, 2, 4]"),
     *          @OA\Property(property="investigations", type="string", format="array", example="[1, 2, 4]"),
     *          @OA\Property(property="treatment_procedures", type="string", format="array", example="[1, 2, 4]"),
     *          @OA\Property(property="drugs", type="string", format="array", example="[1, 2, 4]"),
     *    )
     * ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
    */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->consultationRepository->UpdateById($id, $request->all()));
    }

    /**
     * @OA\Delete(
     ** path="/consultations/{id}",
     *   tags={"[Tenant] Consultations"},
     *   summary="Update new Consultations",
     *   operationId="UpdateConsultations",
     *   security={ {"bearer": {}} },
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * )
     **/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, $id)
    {
        return response()->json($this->consultationRepository->destroy($id))->setStatusCode(204);
    }
}
