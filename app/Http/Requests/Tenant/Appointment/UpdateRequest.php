<?php

namespace App\Http\Requests\Tenant\Appointment;

use App\Http\Requests\ApiRequest;
use App\Models\Tenant\Appointment;
use App\Policies\Tenant\AppointmentPolicy;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new AppointmentPolicy(request()->header('x-tenant'), 'appointment.update');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $appointment_id = $this->route()->parameters('appointments');
        $appointment = $this->findRecordInRequest(Appointment::class, $appointment_id['appointment']);

        return [
            'first_name'     => 'required_without:patient_id',
            'last_name'      => 'required_without:patient_id',
            'dni'            => 'required_without:patient_id|unique:people,dni,'.$appointment->patient->person_id,
            'urgency'        => 'sometimes|boolean',
            'scheduled_date' => 'required',
            'patient_id'     => 'required_without:dni|exists:patients,id',
            'user_id'        => 'required|exists:users,id'
        ];
    }
}
