<?php

namespace Database\Seeders\Tenant;

use App\Models\Tenant\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::findByName('Administrador');
        $permissions = DB::table('permissions')->pluck('name')->toArray();

        $role->givePermissionTo($permissions);

        $user = User::findOrFail(1);
        $user->assignRole($role);
    }
}
