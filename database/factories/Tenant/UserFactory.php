<?php

namespace Database\Factories\Tenant;

use App\Models\Tenant\User;
use App\Models\Central\CentralUser;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $central_user = CentralUser::find(1);

        return [
            'global_id'  => $central_user->global_id,
            'first_name' => $this->faker->name,
            'last_name'  => $this->faker->lastName,
            'dni' => $this->faker->unique()->ean8,
            'email' => $this->faker->unique()->safeEmail,
            'password' => bcrypt('secret'),
            'is_active' => true,
            'address' => [
                'street'  => $this->faker->address,
                'country' => $this->faker->country,
                'state'   => $this->faker->state,
                'city'    => $this->faker->city
            ],
            'phone'      => [
                'landline' => $this->faker->e164PhoneNumber,
                'movil'    => $this->faker->e164PhoneNumber
            ],
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
