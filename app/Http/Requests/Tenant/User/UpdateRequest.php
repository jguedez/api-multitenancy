<?php

namespace App\Http\Requests\Tenant\User;

use App\Http\Requests\ApiRequest;
use App\Policies\Tenant\UserPolicy;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $policy = new UserPolicy(request()->header('x-tenant'), 'user.update');
        return $policy->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $router = $this->route()->parameters('users');
        $user_id = (int)$router['user'];

        return [
            'global_id'         => 'required|exists:users,global_id',
            'email'             => 'required|email|unique:users,email,'.$user_id,
            'password'          => 'required|string|min:6',
            'first_name'        => 'required|string',
            'last_name'         => 'required|string',
            'dni'               => 'required|unique:users,dni,'.$user_id,
            'specialties'       => 'sometimes|exists:specialties,id',
            'schedules'         => 'required',
            'schedules.day'     => 'required_with:schedules|exists:days,id',
            'schedules.schedule'=> 'required_with:schedules|exists:schedules,id',
            'phone'             => 'sometimes',
            'phone.landline'    => 'required_with:phone',
            'phone.movil'       => 'required_with:phone',
            'address'           => 'sometimes',
            'address.street'    => 'required_with:address',
            'address.country'   => 'required_with:address',
            'address.state'     => 'required_with:address',
            'address.city'      => 'required_with:address',
        ];
    }
}
