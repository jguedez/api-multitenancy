<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;
use App\Repositories\Tenant\ComplaintRepository;
use App\Http\Requests\Tenant\Complaint\ShowRequest;
use App\Http\Requests\Tenant\Complaint\IndexRequest;
use App\Http\Requests\Tenant\Complaint\StoreRequest;
use App\Http\Requests\Tenant\Complaint\UpdateRequest;
use App\Http\Requests\Tenant\Complaint\DestroyRequest;

class ComplaintController extends Controller
{
    protected $complaintRepository;

    /**
     * Complaint Constructor.
     *
     * @param ComplaintRepository $complaintRepository
     */

    public function __construct(ComplaintRepository $complaintRepository)
    {
      $this->complaintRepository = $complaintRepository;
    }

    /**
     * @OA\Get(
     *   path="/complaints",
     *   tags={"[Tenant] Complaints"},
     *   summary="Lists all Complaints associated with tenant",
     *   operationId="IndexComplaints",
     *   security={ {"bearer": {}} },
     *
     * @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * )
     **/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        return response()->json($this->complaintRepository->getAll($request->all()));
    }


    /**
     * @OA\Post(
     ** path="/complaints",
     *   tags={"[Tenant] Complaints"},
     *   summary="Store new Complaints",
     *   operationId="StoreComplaints",
     *   security={ {"bearer": {}} },
     *
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for create new Complaints",
     *     @OA\JsonContent(
     *          required={"name","description"},
     *          @OA\Property(property="name", type="string", format="string", example="Dolor Intramuscular"),
     *          @OA\Property(property="description", type="string", format="string", example="SAJDKASJKDJSAKDJASKD")
     *    )
     * ),
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
    */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return response()->json($this->complaintRepository->create($request->all()))->setStatusCode(201);
    }

    /**
     * @OA\Get(
     *   path="/complaints/{id}",
     *   tags={"[Tenant] Complaints"},
     *   summary="Details Complaints",
     *   operationId="ShowComplaints",
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *  security={ {"bearer": {}} }
     * )
     **/
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, $id)
    {
        return response()->json($this->complaintRepository->getById($id));
    }

    /**
     * @OA\Put(
     ** path="/complaints/{id}",
     *   tags={"[Tenant] Complaints"},
     *   summary="Update new Complaints",
     *   operationId="UpdateComplaints",
     *   security={ {"bearer": {}} },
     *
     *  @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Parameter(
     *      name="X-Tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\RequestBody(
     *      required=true,
     *      description="fields for update new Complaints",
     *     @OA\JsonContent(
     *          required={"name","description"},
     *          @OA\Property(property="name", type="string", format="string", example="Dolor Rodilla"),
     *          @OA\Property(property="description", type="string", format="string", example="SAJDKASJKDJSAKDJASKD")
     *    )
     * ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *       content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                          "empty"
     *                       }
     *                 )
     *             )
     *         }
     *     ),
     *  @OA\Response(
     *      response=412,
     *       description="Pre-condition Failed",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )),
     *)
     *
    */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->complaintRepository->UpdateById($id, $request->all()));
    }

    /**
     * @OA\Delete(
     ** path="/complaints/{id}",
     *   tags={"[Tenant] Complaints"},
     *   summary="Update new Complaints",
     *   operationId="UpdateComplaints",
     *   security={ {"bearer": {}} },
     *
     * @OA\Parameter(
     *      name="X-tenant",
     *      in="header",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *  @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * )
     **/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, $id)
    {
        return response()->json($this->complaintRepository->destroy($id))->setStatusCode(204);
    }
}
