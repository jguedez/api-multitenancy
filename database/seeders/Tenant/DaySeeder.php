<?php

namespace Database\Seeders\Tenant;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('days')->insert([[
            'name' => 'Lunes'
        ],
        [
            'name' => 'Martes'
        ],
        [
            'name' => 'Miercoles'
        ],
        [
            'name' => 'Jueves'
        ],
        [
            'name' => 'Viernes'
        ],
        [
            'name' => 'Sabado'
        ],
        [
            'name' => 'Domingo'
        ]]);
    }
}
