<?php

namespace Database\Factories\Tenant;

use App\Models\Tenant\TreatmentProcedure;
use Illuminate\Database\Eloquent\Factories\Factory;

class TreatmentProcedureFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TreatmentProcedure::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type'     => $this->faker->word,
            'name'     => $this->faker->word,
            'comments' => $this->faker->sentence(6),
            'created_at'  => now(),
            'updated_at'  => now(),
        ];
    }
}
